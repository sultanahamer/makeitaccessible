# App to access Android phone via voice and other means

## Latest debug app

Latest debug app can be found [here](https://gitlab.com/sultanahamer/makeitaccessible/-/jobs/artifacts/master/raw/app/build/outputs/apk/debug/app-debug.apk?job=assembleDebug)

## Voice
### Whats implemented so far
- Navigate across any given screen
- Click/Tap on buttons, images or any other clickable content
- Launch any installed app
- Perform global actions like going back, going home, recent apps
- Write text / number or even dictate characters, press submit/enter on keypad
- Scroll content
- Find editable nodes 
- Announce multiple nodes at once and choose from them
- Always stay awake
- Custom Workflows - create custom commands using basic commands to automate regular workflows
- Add custom delays in workflows
- Simple sleep mode implemented
- Input punctuation
- 3rd party Apps can add their custom voice commands
- Highlighter can be hidden while doing sensitive(passwords etc) input
- Record voice commands to create a workflow
- Pause when on a phone call or music/video starts playing
- Rectangle highlight should be refreshed as UI gets updated
- Debug command to hear what was previous command app heard
- Workflow reuse in another workflow
- Jump multiple nodes at once like two times go next, 5 go next


### Yet to implement
- Settings screen for user to set preferred strings for any commands
- All the user interaction should be standardized like announcing result succ/fail, asking for user interaction - with this, we can standardize stuff across different modes of interaction like in silent mode how do we do all these, with keypad input how do we do this etc
- Expand / Alias input during voice input
- Implement did you mean kind of options for failed voice recognition 
- Keyword based voice command recognition rather than exact sentence
- Implement voice search with some ai api
- Reject AppActions if app identifier is differing next time
- Reject AppActions error message if app identifier is differing
- Separate Workflow names vs voice command to start workflow as of now both are same
- Implement ListView in EditWorkflow activity
- Choose node etc aren't 1 action in workflow yet, less effort
- help command that tells what state we are in and what can be done next
- Choose indexed should handle more than 8 things paginated
- GLOBAL_ACTION_KEYCODE_HEADSETHOOK, sounds interesting, lets have a look at this later
- Complete solution to work with bluetooth headset inputs

### Bugs

## Naans
### Context: Naan(translates to Grandmother's in our language) for people with difficulty in close eyesight and pressing keys on keypad phones
### Whats implemented
- Naan's service can now take actions from companion app and let users operate their phone

### Yet to implement
- Pick up call
- End call

## Nitpicks
- Translate all conversations to functional. Easier to work around going forward
- Every action performed should give a voice over

## Refactoring observations
- Resume conversations should be called only for the ones where they are jumped abruptly from, for now lets hard code it for notification conversation
