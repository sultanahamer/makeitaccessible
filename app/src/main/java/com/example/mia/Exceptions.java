package com.example.mia;

public class Exceptions {
    public static class CommandNotFoundException extends Exception{ }
    public static class WorkflowAlreadyExists extends Exception{ }
    public static class WorkflowNotFound extends Exception{ }
}
