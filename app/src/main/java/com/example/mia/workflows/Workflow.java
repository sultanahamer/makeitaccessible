package com.example.mia.workflows;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.mia.Exceptions;
import com.github.underscore.U;
import com.google.gson.Gson;

import java.util.List;
import java.util.Objects;

public class Workflow {
    public String title;
    public List<Action> actions;

    public Workflow(String title, List<Action> actions) {
        this.title = title;
        this.actions = actions;
    }
    private static boolean allValidCommands(Workflow workflow){
        return !U.chain(workflow.actions)
                .map(action -> action.command)
                .filter(command -> command == null)
                .value()
                .isEmpty();
    }

    public static Workflow createWorkflow(String stringifiedWorkflow) throws Exceptions.CommandNotFoundException {
        Workflow workflow = new Gson().fromJson(stringifiedWorkflow, Workflow.class);
        if(allValidCommands(workflow)) throw new Exceptions.CommandNotFoundException();
        return workflow;
    }

    public static Workflow createWorkflow(String title, List<String> voiceCommands) {
        return new Workflow(title, U.map(voiceCommands, Action::new));
    }

    public static String stringifyWorkflow(Workflow workflow) {
        return new Gson().toJson(workflow);
    }

    @NonNull
    @Override
    public String toString() {
        return title;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj == null) return false;

        if(super.equals(obj)) return true;

        if(!obj.getClass().equals(Workflow.class)) return false;

        Workflow otherWorkflow = (Workflow) obj;
        return Objects.equals(otherWorkflow.title, this.title);
    }
}
