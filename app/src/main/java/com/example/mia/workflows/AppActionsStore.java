package com.example.mia.workflows;

import static com.example.mia.appactions.AppAction.ActionType.NAANS;
import static com.example.mia.appactions.AppAction.ActionType.VOICE;
import static com.example.mia.utils.SharedPrefs.getAllAppActions;
import static com.example.mia.utils.SharedPrefs.saveActions;

import android.content.Context;

import androidx.annotation.Nullable;

import com.example.mia.appactions.AppAction;
import com.example.mia.command.CommandUtils;
import com.example.mia.utils.ListUtils;
import com.github.underscore.U;

import java.util.ArrayList;
import java.util.List;

public class AppActionsStore {
    private static List<AppAction> appActions;

    public static List<AppAction> getAllActions(Context context) {
        if(appActions == null) {
            appActions = getAllAppActions(context);
        }
        return new ArrayList<>(appActions);
    }

    public static List<AppAction> getVoiceActions(Context context) {
        return U.filter(getAllAppActions(context), action -> action.actionType == VOICE);
    }

    public static List<AppAction> getNaansActions(Context context) {
        return U.filter(getAllAppActions(context), action -> action.actionType == NAANS);
    }

    public static void updateAppActions(String appName, List<AppAction> actions, Context context){
        // TODO: make sure no other action has the same voice input
//        if(items.contains(workflow)) {
//            Toast.makeText(context, "Workflow with this title already exists", Toast.LENGTH_LONG).show();
//            return;
//        }
       saveActions(actions, appName, context);
       appActions = getAllAppActions(context);
    }

    @Nullable
    public static AppAction getAppAction(String voiceCommand, Context context) {
        return ListUtils.find(getVoiceActions(context), action ->
                CommandUtils.matches(voiceCommand, action.defaultVoiceInput
                ));
    }

}
