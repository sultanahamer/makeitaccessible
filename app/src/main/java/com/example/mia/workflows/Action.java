package com.example.mia.workflows;

import static android.text.TextUtils.isEmpty;

import static com.example.mia.Command.INPUT_TEXT_TYPE_TEXT;
import static com.example.mia.Command.PROCESS_VOICE_INPUT;
import static java.util.Arrays.asList;


import com.example.mia.Command;

import java.util.List;

public class Action {
    public Command command;
    public String param;

    public DynamicInput dynamicInput;
    public boolean shouldDisableTts = true;

    public int delayForNextActionInMillis;
    public static List<Integer> allowedDelays = asList(50, 100, 200, 500, 1000, 2000, 3000, 5000);

    public static boolean hasDynamicInput(Action action){
        return action != null && action.dynamicInput != null && !isEmpty(action.dynamicInput.question);
    }

    public Action(String voiceCommand) {
        this(PROCESS_VOICE_INPUT, voiceCommand);
    }

    public Action(Command command, String param) {
        this(command, param, null);
    }

    public Action(Command command, String param, DynamicInput dynamicInput) {
        this(command, param, 50, dynamicInput);
    }

    public Action(Command command, String param, int delayForNextActionInMillis, DynamicInput dynamicInput) {
        this.command = command;
        this.param = param;
        this.delayForNextActionInMillis = delayForNextActionInMillis;
        this.dynamicInput = dynamicInput;
    }

    public static class DynamicInput {
        public String question;
        public Command inputType;

        public DynamicInput(String question, Command inputType) {
            this.question = question == null ? "" : question;
            this.inputType = inputType == null ? INPUT_TEXT_TYPE_TEXT : inputType;
        }
    }

}
