package com.example.mia.workflows;

import static com.example.mia.utils.Lodash.mapNonNull;
import static com.example.mia.utils.SharedPrefs.getAllStringifiedWorkflows;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.mia.Exceptions;
import com.example.mia.Exceptions.WorkflowAlreadyExists;
import com.example.mia.Exceptions.WorkflowNotFound;
import com.example.mia.R;
import com.example.mia.StoreChangeListener;
import com.example.mia.utils.ListUtils;
import com.example.mia.utils.SharedPrefs;
import com.github.underscore.U;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WorkflowStore {
    private static List<Workflow> workflows;
    private static final Set<StoreChangeListener<Workflow>> listeners = new HashSet<>(3);
    public static void addListener(StoreChangeListener<Workflow> listener){
        listeners.add(listener);
    }

    public static void removeListener(StoreChangeListener<Workflow> listener){
        listeners.remove(listener);
    }
    public static List<Workflow> getAllWorkflows(Context context) {
        if(workflows != null) return new ArrayList<>(workflows);
        workflows = new ArrayList<>();
        List<Workflow> workflowsLoaded = mapNonNull(stringifiedWorkflow -> {
            try {
                return Workflow.createWorkflow(stringifiedWorkflow);
            } catch (Exception exception) {
                Toast.makeText(context, R.string.couldnt_load_a_workflow, Toast.LENGTH_LONG).show();
                return null;
            }
        }, getAllStringifiedWorkflows(context));
        workflows.addAll(workflowsLoaded);
        return new ArrayList<>(workflows);
    }

    public static void addWorkflow(Workflow workflow, Context context) throws WorkflowAlreadyExists {
        if(workflows.contains(workflow)) {
            throw new WorkflowAlreadyExists();
        }
        workflows.add(workflow);
        SharedPrefs.saveAllStringifiedWorkflows(U.map(workflows, Workflow::stringifyWorkflow), context);
        notifyListeners();
    }

    public static void updateWorkflow(Workflow workflow, String originalWorkflowTitle, Context context) throws WorkflowNotFound, WorkflowAlreadyExists {
        int index = workflows.indexOf(new Workflow(originalWorkflowTitle, null));
        if(index == -1) {
            throw new WorkflowNotFound();
        }
        if(!workflow.title.equals(originalWorkflowTitle) && workflows.contains(new Workflow(workflow.title, null))){
            throw new WorkflowAlreadyExists();
        };
        workflows.remove(index);
        workflows.add(index, workflow);
        SharedPrefs.saveAllStringifiedWorkflows(U.map(workflows, Workflow::stringifyWorkflow), context);
        notifyListeners();
    }

    @Nullable
    public static Workflow getWorkflow(String title, Context context) {
        return ListUtils.find(getAllWorkflows(context), workflow -> workflow.title.equalsIgnoreCase(title));
    }

    public static void notifyListeners(){
        U.forEach(listeners, listener -> {
            if (listener == null) return;
            listener.refresh(new ArrayList<>(workflows));
        });

    }

}
