package com.example.mia;

import static android.text.TextUtils.isEmpty;
import static com.example.mia.command.CommandDefinition.CommandFlags.HAS_VARIABLES;
import static com.example.mia.command.CommandDefinition.CommandFlags.IS_CONVERSATION;
import static com.example.mia.command.CommandDefinition.CommandFlags.REQUIRES_NODE;
import static java.util.Arrays.asList;

import android.content.Context;

import com.example.mia.command.CommandDefinition;
import com.example.mia.command.Punctuation;
import com.github.underscore.Tuple;
import com.github.underscore.U;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Command {
    OUT(new CommandDefinition(R.string.out_command, REQUIRES_NODE.value)),
    IN(new CommandDefinition(R.string.in_command, REQUIRES_NODE.value)),
    NEXT(new CommandDefinition(R.string.next_command, REQUIRES_NODE.value)),
    PREV(new CommandDefinition(R.string.prev_command, REQUIRES_NODE.value)),
    SCROLL_UP(new CommandDefinition(R.string.scroll_up_command, REQUIRES_NODE.value)),
    SCROLL_DOWN(new CommandDefinition(R.string.scroll_down_command, REQUIRES_NODE.value)),
    TAP(new CommandDefinition(R.string.tap_command, REQUIRES_NODE.value)),
    START_EXPLORE_UI(R.string.start_explore_ui),
    STOP_EXPLORE_UI(R.string.stop_explore_ui),
    START_RECORDING(R.string.start_recording),
    STOP_RECORDING(R.string.stop_recording),
    START_HIGHLIGHT(R.string.start_highlighter),
    STOP_HIGHLIGHT(R.string.stop_highlighter),

    LONG_TAP(new CommandDefinition(R.string.long_tap_command, REQUIRES_NODE.value)),
    CONFIRM(R.string.confirm_command),
    NO(R.string.no_command),
    CANCEL(R.string.cancel_command),
    PROCESS_VOICE_INPUT(R.string.process_voice_input),
    CHOOSE(R.string.choose_command),

    INPUT_TEXT_TYPE_NUMBER(R.string.input_text_type_number_command),
    INPUT_TEXT_TYPE_TEXT(R.string.input_text_type_text_command),
    INPUT_TEXT_TYPE_CHARACTER(R.string.input_text_type_character_command),
    INPUT_TEXT_TYPE_PUNCTUATION(R.string.input_text_type_punctuation),

    START_TEXT_INPUT_CONV(new CommandDefinition(R.string.start_text_input_command, REQUIRES_NODE.value | IS_CONVERSATION.value)),
    CHOOSE_NODE_CONV(new CommandDefinition(R.string.choose_node_start, IS_CONVERSATION.value)),
    LAUNCH_APP_CONV(new CommandDefinition(R.string.launch_app_start, IS_CONVERSATION.value)),
    RECENT_NOTIFICATION(new CommandDefinition(R.string.recent_notification, IS_CONVERSATION.value)),
    SLEEP_CONV(new CommandDefinition(R.string.sleep, IS_CONVERSATION.value)),
    LAUNCH_WORKFLOW_CONV(new CommandDefinition(R.string.launch_workflow_start, IS_CONVERSATION.value)),
    CHOOSE_EDITABLE_NODE_CONV(new CommandDefinition(R.string.choose_editable_node_start, IS_CONVERSATION.value)),
    CHOOSE_NODE_WITH_TEXT_CONV(new CommandDefinition(R.string.choose_node_with_text_start, IS_CONVERSATION.value)),

    TAP_NODE_WITH_TEXT(new CommandDefinition(R.string.tap_node_with_text, IS_CONVERSATION.value | HAS_VARIABLES.value)),

    WAKE_UP(R.string.wakeup),

    SUBMIT_TEXT(R.string.submit_text),
    REPEAT(R.string.repeat),
    DEBUG_LAST_COMMAND(R.string.debug_last_command),
    DO_MULTIPLE_TIMES(new CommandDefinition(R.string.do_multiple_times, HAS_VARIABLES.value)),

    INPUT_TEXT(R.string.input_text),
    CHOOSE_NODE(R.string.choose_node),
    CHOOSE_WORKFLOW(R.string.choose_node), //TODO: check and refactor / remove the ones we don't require
    LAUNCH_APP(R.string.launch_app),
    CHOOSE_NODE_WITH_TEXT(R.string.choose_node_with_text),
    GLOBAL_HOME(R.string.global_home),
    GLOBAL_RECENT_APPS(R.string.global_recent_apps),
    GLOBAL_BACK(R.string.global_back),

    PUNCTUATION_SPACE(new Punctuation(R.string.punctuation_space, " ")),
    PUNCTUATION_AT(new Punctuation(R.string.punctuation_at, "@")),
    PUNCTUATION_EXCLAMATION(new Punctuation(R.string.punctuation_exclamation, "!")),
    PUNCTUATION_HASH(new Punctuation(R.string.punctuation_hash, "#")),
    PUNCTUATION_DOLLAR(new Punctuation(R.string.punctuation_dollar, "$")),
    PUNCTUATION_PERCENT(new Punctuation(R.string.punctuation_percent, "%")),
    PUNCTUATION_CAP(new Punctuation(R.string.punctuation_cap, "^")),
    PUNCTUATION_AND(new Punctuation(R.string.punctuation_and, "&" )),
    PUNCTUATION_STAR(new Punctuation(R.string.punctuation_star, "*" )),
    PUNCTUATION_OPEN_PARENTHESIS(new Punctuation(R.string.punctuation_open_parenthesis, "(")),
    PUNCTUATION_CLOSE_PARENTHESIS(new Punctuation(R.string.punctuation_close_parenthesis, ")")),
    PUNCTUATION_HYPHEN(new Punctuation(R.string.punctuation_hyphen, "-")),
    PUNCTUATION_EQUALS(new Punctuation(R.string.punctuation_equals, "=")),
    PUNCTUATION_PLUS(new Punctuation(R.string.punctuation_plus, "+")),
    PUNCTUATION_SLASH(new Punctuation(R.string.punctuation_slash, "/")),
    PUNCTUATION_GREATER(new Punctuation(R.string.punctuation_greater, ">")),
    PUNCTUATION_LESSER(new Punctuation(R.string.punctuation_lesser, "<")),

    DO_NOTHING(R.string.do_nothing_command) // TODO: might not be useful, lets see
    ;

    public final CommandDefinition commandDefinition;

    public static final List<Command> textInputTypes = asList(INPUT_TEXT_TYPE_TEXT, INPUT_TEXT_TYPE_CHARACTER, INPUT_TEXT_TYPE_NUMBER);
    public static final List<Command> WORKFLOW_SUPPORTED_COMMANDS = asList(PROCESS_VOICE_INPUT);
    Command(CommandDefinition commandDefinition) {
        this.commandDefinition = commandDefinition;
    }
    Command(int commandText) {
        this.commandDefinition = new CommandDefinition(commandText);
    }

    public static String getTextFor(Command command, Context context) {
        return context.getString(command.commandDefinition.defaultEasyForVoiceRecognitionWord);
    }

    public static Map<String, Command> createTextToCommandMapping(Context context){
        Map<String, Command> textToCommandMapping = new HashMap<>();
        for(Command command : Command.class.getEnumConstants()) {
            String text = context.getString(command.commandDefinition.defaultEasyForVoiceRecognitionWord);
            if(isEmpty(text)) continue;
            textToCommandMapping.put(text, command);
            command.commandDefinition.template = text;
        }
        return textToCommandMapping;
    }
    private static Map<String, Command> textToCommandMapping;

    public static Map<String, Command> getCachedTextToCommandMapping(Context context) {
        if(textToCommandMapping != null) return textToCommandMapping;
        return createTextToCommandMapping(context);
    }

    public static final Map<String, Command> commandNameToCommandMapping = computeCommandNameToCommandMapping();
    public static Map<String, Command> computeCommandNameToCommandMapping(){
        List<Tuple<String, Command>> blah = U.map(asList(Command.class.getEnumConstants()), command -> new Tuple<>(command.toString(), command));
        return U.toMap(blah);
    }
    public static String punctuation(String symbol, Context context) {
        Command command = getCachedTextToCommandMapping(context).get(symbol);
        if(command == null || ! (command.commandDefinition instanceof Punctuation)) return "";
        return ((Punctuation) command.commandDefinition).symbol;
    }
}
