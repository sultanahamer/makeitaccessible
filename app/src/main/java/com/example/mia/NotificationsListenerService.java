package com.example.mia;

import static com.example.mia.utils.Async.runOnMainDelayed;
import static java.util.Arrays.asList;

import android.content.Context;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import com.github.underscore.Consumer;
import com.github.underscore.Optional;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NotificationsListenerService extends NotificationListenerService {

    private static WeakReference<NotificationsListenerService> currentInstanceRef;
    private static List<Consumer<StatusBarNotification>> listeners = new ArrayList<>(0);
    @Override
    public void onNotificationPosted(StatusBarNotification sbn, RankingMap rankingMap) {
        System.out.println("yolo notiication app name" + sbn.getPackageName());
        System.out.println("yolo notiication title" + sbn.getNotification().extras.getString("android.title"));
        System.out.println("yolo notiication text" + sbn.getNotification().extras.getString("android.text"));
//        System.out.println("yolo notiication actions" + actionsAvailable);
//        U.forEach(listeners, listener -> listener.accept(sbn));
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn, RankingMap rankingMap) {
        super.onNotificationRemoved(sbn, rankingMap);
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        currentInstanceRef = new WeakReference<>(this);
    }

    @Override
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
        currentInstanceRef = null;
    }

    @Override
    public void onListenerHintsChanged(int hints) {
        super.onListenerHintsChanged(hints);
    }

    public static boolean hasWorkingInstance() {
        return currentInstanceRef.get() != null;
    }

    public List<StatusBarNotification> getAllNotifications(Context context){
        return asList(getActiveNotifications());
    }

    public static Optional<StatusBarNotification> getLatestNotification(Context context){
        if(!hasWorkingInstance()) return Optional.absent();
        List<StatusBarNotification> allNotifications = currentInstanceRef.get().getAllNotifications(context);
        if(allNotifications.isEmpty()) return Optional.absent();
        return Optional.of(allNotifications.get(0));
    }

    public static void dismissNotification(StatusBarNotification notification) {
        if(!hasWorkingInstance()) return;
        currentInstanceRef.get().cancelNotification(notification.getKey());
    }

    public static void registerListener(Consumer<StatusBarNotification> onNotification){
        listeners.add(onNotification);
    }

    public static void deregisterListener(Consumer<StatusBarNotification> onNotification){
        listeners.remove(onNotification);
    }
}
