package com.example.mia.broadcast_receivers;

import static com.example.mia.utils.MessageBus.MessageType.SHOULD_ENTER_SLEEP_MODE;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.RequiresApi;

import com.example.mia.utils.MessageBus;

public class SystemAudioReceiver {

    private static boolean requestJustPlaced;
    private static AudioFocusRequest mFocusRequest;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void requestAudioFocusAndMonitorIfUserPlaysSomeMedia(Context context) {
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        AudioAttributes mPlaybackAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ASSISTANT)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();
        mFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(mPlaybackAttributes)
                .setAcceptsDelayedFocusGain(true)
                .setWillPauseWhenDucked(true)
                .setOnAudioFocusChangeListener(SystemAudioReceiver::onAudioFocusChange, new Handler(Looper.getMainLooper()))
                .build();


        // requesting audio focus
        requestJustPlaced = true;
        int res = mAudioManager.requestAudioFocus(mFocusRequest);
//        synchronized (mFocusLock) {
//            if (res == AudioManager. AUDIOFOCUS_REQUEST_FAILED) {
//                mPlaybackDelayed = false;
//            } else if (res == AudioManager. AUDIOFOCUS_REQUEST_GRANTED) {
//                mPlaybackDelayed = false;
//                playbackNow();
//            } else if (res == AudioManager. AUDIOFOCUS_REQUEST_DELAYED) {
//                mPlaybackDelayed = true;
//            }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void unsubscribeToAudioFocusChanges(Context context) {
        if(mFocusRequest == null) return;
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.abandonAudioFocusRequest(mFocusRequest);
    }
    // implementation of the OnAudioFocusChangeListener
    private static void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                if(requestJustPlaced) break; // first time when request is being processed it is thrown as loss. From next it should be alright
                MessageBus.broadcast(SHOULD_ENTER_SLEEP_MODE);
                break;
        }
        requestJustPlaced = false;
    }
}
