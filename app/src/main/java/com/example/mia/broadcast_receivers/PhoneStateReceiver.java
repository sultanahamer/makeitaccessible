package com.example.mia.broadcast_receivers;

import static android.telephony.TelephonyManager.EXTRA_STATE_OFFHOOK;
import static android.telephony.TelephonyManager.EXTRA_STATE_RINGING;
import static com.example.mia.utils.MessageBus.MessageType.SHOULD_ENTER_SLEEP_MODE;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.example.mia.utils.MessageBus;

public class PhoneStateReceiver extends BroadcastReceiver {
    /*
        Lolipop has a problem of raising these events multiple times leading to multiple
        drawing of caller id, multiple notifications. Ahhhhhhhhhh!!!!!!
     */

    @Override
    public void onReceive(final Context context, Intent intent) {
        String phoneState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (EXTRA_STATE_RINGING.equals(phoneState) || EXTRA_STATE_OFFHOOK.equals(phoneState)) {
            MessageBus.broadcast(SHOULD_ENTER_SLEEP_MODE);
        }
    }
}
