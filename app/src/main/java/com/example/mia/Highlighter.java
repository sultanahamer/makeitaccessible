package com.example.mia;

import static android.content.Context.WINDOW_SERVICE;
import static com.example.mia.utils.Async.runOnMain;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.lang.ref.WeakReference;

public class Highlighter {
    private boolean disable = false;
    private final RectView rectView;
    WeakReference<Context> contextRef;
    private WindowManager wm;
    private int[] highlighterViewScreenBounds = new int[2];

    Highlighter(Context context){
        this.contextRef = new WeakReference<>(context);
        rectView = new RectView(context);
        init();
    }

    public void stopHighlighting() {
        disable = true;
        rectView.clear();
    }

    public void startHighlighting() {
        disable = false;
    }

    private void init() {
        Context context = contextRef.get();
        disable = false;
        wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.type = WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY;
        lp.format = PixelFormat.TRANSLUCENT;
        lp.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        lp.flags |= WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
        lp.flags |= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.height = ViewGroup.LayoutParams.MATCH_PARENT;

        wm.addView(rectView, lp);
    }

    public void highlight(Rect viewsBounds) {
        if(disable) return;
        Log.d("highlighter yolo", "highlighting " + viewsBounds);
        runOnMain(() -> {
            rectView.getLocationOnScreen(highlighterViewScreenBounds);
            Log.d("highligter yolo", " view bounds should be 0,0 but they are:  " + highlighterViewScreenBounds[0] + ", " + highlighterViewScreenBounds[1]);
            Rect adjustedBounds = new Rect();
            adjustedBounds.top = viewsBounds.top - highlighterViewScreenBounds[1];
            adjustedBounds.bottom = viewsBounds.bottom - highlighterViewScreenBounds[1];
            adjustedBounds.left = viewsBounds.left - highlighterViewScreenBounds[0];
            adjustedBounds.right = viewsBounds.right - highlighterViewScreenBounds[0];
            rectView.highlight(adjustedBounds);
        });
    }
    public void removeHighlight(){
        runOnMain(rectView::clear);
    }

    public class RectView extends View {

        private final Paint borderPaint;
        private Rect border;

        public RectView(Context context) {
            super(context);
            borderPaint = new Paint();
            borderPaint.setColor(Color.RED);
            borderPaint.setStyle(Paint.Style.STROKE);
            borderPaint.setStrokeWidth(
                    context.getResources().getDimensionPixelSize(R.dimen.highlight_overlay_border));
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            Log.d("yolo overlay", "overlay clicked");
            return super.dispatchTouchEvent(event);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            if(border == null){
                return;
            }
            canvas.drawRect(border, borderPaint);
        }

        public void highlight(Rect rect){
            border = rect;
            invalidate();
        }

        public void clear(){
            border = null;
            invalidate();
        }
    }
}
