package com.example.mia.command;

import static com.example.mia.utils.Functional.mapIndexes;
import static com.example.mia.utils.VoiceInputUtils.processSpecialInput;
import static java.util.Collections.emptyList;

import android.content.Context;
import android.text.TextUtils;

import com.example.mia.Command;

import java.util.List;
import java.util.regex.Pattern;

public class CommandUtils {
    public final static String VARIABLES_REGEX = ":\\d";
    public final static String VARIABLES_MATCHING_REGEX = ".*:\\d.*";
    public final static String CAPTURE_GROUP_REGEX = "(.*)";

    public static boolean matches(String text, Command command) {
        if(TextUtils.isEmpty(command.commandDefinition.template)) return false;
        if(!command.commandDefinition.hasVariables) return command.commandDefinition.template.equals(text);
        return text.matches(command.commandDefinition.template.replaceAll(VARIABLES_REGEX, CAPTURE_GROUP_REGEX));
    }

    public static boolean matches(String text, String template) {
        if(TextUtils.isEmpty(template)) return false;
        return text.matches(template.replaceAll(VARIABLES_REGEX, CAPTURE_GROUP_REGEX));
    }


    public static boolean hasVariables(String template){
        return template.matches(VARIABLES_MATCHING_REGEX);
    }
    public static List<String> getVariables(Command command, String input, Context context) {
        String regexPatternString = command.commandDefinition.template.replaceAll(VARIABLES_REGEX, CAPTURE_GROUP_REGEX);
        java.util.regex.Matcher matcher = Pattern.compile(regexPatternString).matcher(input);
        if(!matcher.find()) return emptyList();
        // index + 1 coz 0 index is whole matched string
        return mapIndexes(matcher.groupCount(), index -> processSpecialInput(matcher.group(index + 1), context));
    }
    public static List<String> getVariables(String template, String input, Context context) {
        String regexPatternString = template.replaceAll(VARIABLES_REGEX, CAPTURE_GROUP_REGEX);
        java.util.regex.Matcher matcher = Pattern.compile(regexPatternString).matcher(input);
        if(!matcher.find()) return emptyList();
        // index + 1 coz 0 index is whole matched string
        return mapIndexes(matcher.groupCount(), index -> processSpecialInput(matcher.group(index + 1), context));
    }

}
