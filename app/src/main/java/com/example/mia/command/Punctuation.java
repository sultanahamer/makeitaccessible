package com.example.mia.command;

public class Punctuation extends CommandDefinition {
    public final String symbol;

    public Punctuation(int defaultResource, String symbol) {
        super(defaultResource);
        this.symbol = symbol;
    }
}
