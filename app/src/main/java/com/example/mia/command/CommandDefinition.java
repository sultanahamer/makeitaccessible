package com.example.mia.command;


// commands like punctuation have extra info like symbol that they represent
public class CommandDefinition {
    public  int defaultEasyForVoiceRecognitionWord;
    public String template; // late init
    public boolean hasVariables = false;
    public boolean requiresNode = false;
    public boolean isConversation = false;
    public enum CommandFlags {
        HAS_VARIABLES(1<< 0),
        REQUIRES_NODE(1 << 1),
        IS_CONVERSATION(1 << 2)
        ;

        public final int value;
        CommandFlags(int value){
            this.value = value;
        }
    }

    public CommandDefinition(int defaultEasyForVoiceRecognitionWord){
        this.defaultEasyForVoiceRecognitionWord = defaultEasyForVoiceRecognitionWord;
    }

    public CommandDefinition(int defaultEasyForVoiceRecognitionWord, int flags){
        this.defaultEasyForVoiceRecognitionWord = defaultEasyForVoiceRecognitionWord;
        requiresNode = hasFlag(flags, CommandFlags.REQUIRES_NODE);
        hasVariables = hasFlag(flags, CommandFlags.HAS_VARIABLES);
        isConversation = hasFlag(flags, CommandFlags.IS_CONVERSATION);
    }


    private boolean hasFlag(int flags, CommandFlags specificFlag){
        return (flags & specificFlag.value) > 0;
    }
}
