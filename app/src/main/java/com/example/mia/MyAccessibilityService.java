package com.example.mia;

import static android.view.KeyEvent.FLAG_LONG_PRESS;
import static android.view.KeyEvent.KEYCODE_DPAD_CENTER;
import static android.view.KeyEvent.KEYCODE_DPAD_DOWN;
import static android.view.KeyEvent.KEYCODE_DPAD_LEFT;
import static android.view.KeyEvent.KEYCODE_DPAD_RIGHT;
import static android.view.KeyEvent.KEYCODE_DPAD_UP;
import static android.view.KeyEvent.KEYCODE_ENTER;
import static android.view.accessibility.AccessibilityEvent.TYPES_ALL_MASK;
import static android.view.accessibility.AccessibilityEvent.TYPE_VIEW_FOCUSED;
import static android.view.accessibility.AccessibilityEvent.TYPE_VIEW_SCROLLED;
import static android.view.accessibility.AccessibilityEvent.TYPE_WINDOWS_CHANGED;
import static android.view.accessibility.AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
import static android.view.accessibility.AccessibilityEvent.eventTypeToString;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_LONG_CLICK;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_BACKWARD;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_FORWARD;
import static com.example.mia.Command.DO_MULTIPLE_TIMES;
import static com.example.mia.Command.GLOBAL_BACK;
import static com.example.mia.Command.GLOBAL_HOME;
import static com.example.mia.Command.GLOBAL_RECENT_APPS;
import static com.example.mia.Command.IN;
import static com.example.mia.Command.INPUT_TEXT;
import static com.example.mia.Command.LAUNCH_APP;
import static com.example.mia.Command.LONG_TAP;
import static com.example.mia.Command.NEXT;
import static com.example.mia.Command.OUT;
import static com.example.mia.Command.PREV;
import static com.example.mia.Command.SCROLL_DOWN;
import static com.example.mia.Command.SCROLL_UP;
import static com.example.mia.Command.START_EXPLORE_UI;
import static com.example.mia.Command.START_HIGHLIGHT;
import static com.example.mia.Command.START_RECORDING;
import static com.example.mia.Command.STOP_EXPLORE_UI;
import static com.example.mia.Command.STOP_HIGHLIGHT;
import static com.example.mia.Command.STOP_RECORDING;
import static com.example.mia.Command.SUBMIT_TEXT;
import static com.example.mia.Command.TAP;
import static com.example.mia.broadcast_receivers.SystemAudioReceiver.requestAudioFocusAndMonitorIfUserPlaysSomeMedia;
import static com.example.mia.broadcast_receivers.SystemAudioReceiver.unsubscribeToAudioFocusChanges;
import static com.example.mia.conversations.NotificationConversation.clearYetToHandleNotifications;
import static com.example.mia.utils.AccessibilityNodeUtils.fillText;
import static com.example.mia.utils.AccessibilityNodeUtils.firstFocusableChild;
import static com.example.mia.utils.AccessibilityNodeUtils.firstFocusableNode;
import static com.example.mia.conversations.ConversationProcessor.enterSleepMode;
import static com.example.mia.utils.AccessibilityNodeUtils.firstFocusableParent;
import static com.example.mia.utils.AccessibilityNodeUtils.getParentOrSelfMatchingCriteria;
import static com.example.mia.utils.AccessibilityNodeUtils.getSealedInstance;
import static com.example.mia.utils.AccessibilityNodeUtils.isValidNode;
import static com.example.mia.utils.AccessibilityNodeUtils.nodeMetaText;
import static com.example.mia.utils.AccessibilityNodeUtils.siblingNext;
import static com.example.mia.utils.AccessibilityNodeUtils.siblingPrevious;
import static com.example.mia.utils.Async.runAsync;
import static com.example.mia.utils.Async.runOnMainDelayed;
import static com.example.mia.utils.Digits.getNumber;
import static com.example.mia.utils.Java.safeCall;
import static com.example.mia.utils.Java.safeFunction;
import static com.example.mia.utils.MessageBus.MessageType.APP_ACTION_ERROR;
import static com.example.mia.utils.MessageBus.MessageType.ENTERING_SLEEP_MODE;
import static com.example.mia.utils.MessageBus.MessageType.EXITING_SLEEP_MODE;
import static com.example.mia.utils.MessageBus.MessageType.SHOULD_ENTER_SLEEP_MODE;
import static com.example.mia.utils.Tts.genericTts;
import static com.example.mia.utils.Tts.speakAfterQueue;
import static com.example.mia.utils.Tts.speakCancellingQueue;
import static com.example.mia.utils.WakeLockUtils.acquireScreenWakeLock;
import static com.example.mia.utils.WakeLockUtils.removeScreenWakeLock;

import static java.util.Arrays.asList;

import android.accessibilityservice.AccessibilityGestureEvent;
import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.graphics.Rect;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.speech.tts.TextToSpeech;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.mia.command.CommandUtils;
import com.example.mia.conversations.ConversationProcessor;
import com.example.mia.utils.AccessibilityNodeUtils;
import com.example.mia.utils.Android;
import com.example.mia.utils.Debounce;
import com.example.mia.utils.MessageBus;
import com.example.mia.utils.Tts;
import com.github.underscore.Consumer;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MyAccessibilityService extends AccessibilityService {


    private TextToSpeech tts;
    private Tts.TtsWrapper genericTtsWrapper;
    private Highlighter highlighter;
    private Rect viewBordersRect = new Rect();
    private int currentWindowId = -1;
    private final Map<Integer, AccessibilityNodeInfo> focusPerWindow = new HashMap<>(10);
    private VoiceInputService voiceInputService;
    private final List<Integer> eventsWhereHighlighterNeedsUpdating = asList(TYPE_VIEW_SCROLLED, TYPE_WINDOW_CONTENT_CHANGED);
    private final Runnable updateHighlightAndNodeIfNeeded = () -> {
        AccessibilityNodeInfo currentNode = currentHighlightedNode();
        if (currentNode == null) focusRelevantNode(genericTtsWrapper);
        else updateHighlightAreaOnScreen(currentNode);
    };

    private final Debounce debouncedUpdateHighlightedNodeIfNeeded = new Debounce(updateHighlightAndNodeIfNeeded, 500);

    private final Consumer<Object> onAppActionError = errorMessage -> {
        Tts.speakAfterQueue(errorMessage.toString(), genericTtsWrapper);
    };

    private final Consumer<Object> onShouldEnterSleepMode = ign -> {
        enterSleepMode(true, genericTtsWrapper, this);
    };

    private final Consumer<Object> onEnteringSleepMode = ignore -> {
        showSoftKeyboard();
        removeScreenWakeLock();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            unsubscribeToAudioFocusChanges(this);
        }
        highlighter.stopHighlighting();
    };

    private final Consumer<Object> onExitingSleepMode = ignore -> {
        hideSoftKeyboard();
        acquireScreenWakeLock(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            requestAudioFocusAndMonitorIfUserPlaysSomeMedia(this);
        }
        highlighter.startHighlighting();
    };

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        safeCall(() -> handleAccessibilityEvent(event), e -> {
            Tts.speakAfterQueue("failed to process accessibility event", genericTtsWrapper);
        });
    }

    private void handleAccessibilityEvent(AccessibilityEvent event) {
        int eventType = event.getEventType();
        Log.d("yolo", ", event type: " + eventTypeToString(eventType));
//        if(eventType == AccessibilityEvent.TYPE_VIEW_HOVER_ENTER) {
//            System.out.println(event);
//            if(event.getSource()!= null) {
//                Rect rect = new Rect();
//                event.getSource().getBoundsInScreen(rect);
//                System.out.println(rect);
//            }
//        }
        if(eventType == AccessibilityEvent.TYPE_VIEW_HOVER_EXIT) {
            if(event.getSource()!=null) {
//                Rect rect = new Rect();
//                event.getSource().getBoundsInScreen(rect);
                AccessibilityNodeInfo sealedNode = getSealedInstance(event.getSource(), getRoot());
                if(sealedNode != null) {
                    highlightNode(sealedNode, genericTtsWrapper);
//                    boolean performed = sealedNode.performAction(ACTION_CLICK);
//                    if(performed) return;
                    return;
                }
//                disableTouchExplorationMode();
//                runOnMainDelayed(() -> {
//                    click(rect.left, rect.top, this::enableTouchExplorationMode);
//                    runOnMainDelayed(this::enableTouchExplorationMode, 300);
//                }, 100);
            }
        }
//        Log.d("yolo", ", action: " + event.getAction()
////                + "windowChanges: " + event.getWindowChanges()
//                + ", contentChangeType: " + event.getContentChangeTypes()
//                + ", app: "+ event.getPackageName());
        if(eventType == TYPE_WINDOWS_CHANGED) {
//            Log.d("yolo", "window changed " +event.getPackageName());
            if(currentWindowId == -1) {
                runOnMainDelayed(() -> focusRelevantNode(genericTtsWrapper), 500);
                currentWindowId = event.getWindowId(); // this isn't accurate windowId, should pick the window which is focused, works for now
                return;
            }
            announceWindowTitleAndFocusRelevantNode(event);
            return;
        }

        if(eventsWhereHighlighterNeedsUpdating.contains(eventType)) {
            debouncedUpdateHighlightedNodeIfNeeded.debouncedRun();
            return;
        }
        AccessibilityNodeInfo eventSource = event.getSource();
//        Log.d("yolo", "view: " + eventSource.getViewIdResourceName());
//        Log.d("yolo", "text: " + eventSource.getText());
//        if(eventType == TYPE_VIEW_CLICKED){
//            Log.d("yolo", "got the click");
//            Log.d("yolo", "view clicked: " + eventSource.getClassName());
//            Log.d("yolo", "view id: " + eventSource.getViewIdResourceName());
//        }
//        if(eventType == TYPE_VIEW_SELECTED){
//            eventSource.getBoundsInScreen(viewBordersRect);
//            Log.d("yolo", "view selected" + viewBordersRect);
////            highlighter.highlight(viewBordersRect);
//            AccessibilityNodeInfo firstFocusableChild = firstFocusableChild(getRootInActiveWindow());
//            if(firstFocusableChild == null) return;
//            highlightNode(firstFocusableChild);
////            firstFocusableChild.performAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS);
//            firstFocusableChild.performAction(AccessibilityNodeInfo.ACTION_FOCUS);
//            Log.d("yolo", "first child" + firstFocusableChild.getContentDescription());
//            return;
//        }
        if(eventType == TYPE_VIEW_FOCUSED && eventSource != null){
            AccessibilityNodeInfo currentHighlightedNode = currentHighlightedNode();
            if(eventSource.equals(currentHighlightedNode)) {
                updateHighlightAreaOnScreen(currentHighlightedNode);
                return;
            }
            AccessibilityNodeInfo sealedInstance = getSealedInstance(eventSource, getRoot());
            if(sealedInstance == null) return;
            highlightNode(sealedInstance, genericTtsWrapper);
        }
    }

    private void disableTouchExplorationMode() {
        AccessibilityServiceInfo serviceInfo = getServiceInfo();
        serviceInfo.flags = serviceInfo.flags & ~(AccessibilityServiceInfo.FLAG_REQUEST_TOUCH_EXPLORATION_MODE);
        setServiceInfo(serviceInfo);
    }

    private void enableTouchExplorationMode() {
        AccessibilityServiceInfo serviceInfo = getServiceInfo();
        serviceInfo.flags = serviceInfo.flags | AccessibilityServiceInfo.FLAG_REQUEST_TOUCH_EXPLORATION_MODE;
        setServiceInfo(serviceInfo);
    }

    @Nullable
    private AccessibilityNodeInfo currentHighlightedNode() {
        AccessibilityNodeInfo root = getRoot();
        if(root == null) return null;
        int windowId = root.getWindowId();
        AccessibilityNodeInfo focusedNode = focusPerWindow.get(windowId);
        return isValidNode(focusedNode) ? focusedNode : null;
    }

    private boolean isDPADButton(int keyCode) {
        return (keyCode == KEYCODE_DPAD_RIGHT || keyCode == KEYCODE_DPAD_CENTER
                || keyCode == KEYCODE_DPAD_UP || keyCode == KEYCODE_DPAD_DOWN
        || keyCode == KEYCODE_DPAD_LEFT || keyCode == KEYCODE_ENTER);
    }
    private void onUserAction(CommandAndArguments commandAndArguments) {
        AccessibilityNodeInfo focusedNode = getFocusedNode();
        Command command = commandAndArguments.command;
        String param = commandAndArguments.param;
        Tts.TtsWrapper ttsWrapper = commandAndArguments.ttsWrapper;
        if (focusedNode == null && command.commandDefinition.requiresNode) {
            focusRelevantNode(ttsWrapper);
            return;
        }
        if (NEXT.equals(command)) {
            highlightNode(siblingNext(focusedNode), ttsWrapper);
            return;
        } else if (IN.equals(command)) {
            highlightNode(firstFocusableChild(focusedNode), ttsWrapper);
            return;
        } else if (SCROLL_DOWN.equals(command)) {
            AccessibilityNodeInfo node = getParentOrSelfMatchingCriteria(getFocusedNode(), AccessibilityNodeInfo::isScrollable);
            if(node == null) Tts.speakCancellingQueue("Not scrollable", ttsWrapper);
            boolean scrolled = node.performAction(ACTION_SCROLL_FORWARD);
            if (scrolled) speakAfterQueue("Scrolled", ttsWrapper);
            return;
        }
        else if (PREV.equals(command)){
            highlightNode(siblingPrevious(focusedNode), ttsWrapper);
            return;
        }
        else if (OUT.equals(command)){
            highlightNode(firstFocusableParent(focusedNode), ttsWrapper);
        }
        else if(SCROLL_UP.equals(command)){
            AccessibilityNodeInfo node = getParentOrSelfMatchingCriteria(getFocusedNode(), AccessibilityNodeInfo::isScrollable);
            if(node == null) Tts.speakCancellingQueue("Not scrollable", ttsWrapper);
            boolean scrolled = node.performAction(ACTION_SCROLL_BACKWARD);
            if(scrolled) speakAfterQueue("Scrolled", ttsWrapper);
            return;
        }
        else if (TAP.equals(command)){
            String nodeDescription = AccessibilityNodeUtils.getText(focusedNode);
            if(!focusedNode.isClickable()) {
                speakCancellingQueue("Not clickable " + nodeDescription, ttsWrapper);
                return;
            }
            boolean performed = focusedNode.performAction(ACTION_CLICK);
            if(!performed) Log.e("yolo", "could not click " + nodeDescription);
            String operationResult = performed ? "Clicked " + nodeDescription : "Could not click " + nodeDescription;
            Tts.speakAfterQueue(operationResult, ttsWrapper);
        }
        else if (LONG_TAP.equals(command)){
            String nodeDescription = AccessibilityNodeUtils.getText(focusedNode);
            if(!focusedNode.isLongClickable()) {
                speakCancellingQueue("Not long clickable " + nodeDescription, ttsWrapper);
                return;
            }
            boolean performed = focusedNode.performAction(ACTION_LONG_CLICK);
            if(!performed) Log.e("yolo", "could not long click " + nodeDescription);
            String operationResult = performed ? "Clicked " + nodeDescription : "Could not long click " + nodeDescription;
            Tts.speakAfterQueue(operationResult, ttsWrapper);
        }
        else if (SUBMIT_TEXT.equals(command)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                focusedNode.performAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_IME_ENTER.getId());
            }
            else {
                speakAfterQueue("can't perform this action, requires Android 11 and above", ttsWrapper);
            }
        }
        else if (START_RECORDING.equals(command)) {
            voiceInputService.startRecording();
            Tts.speakAfterQueue("started recording", ttsWrapper);
            return;
        }
        else if (STOP_RECORDING.equals(command)) {
            voiceInputService.stopRecording();
            return;
        }
        else if(START_EXPLORE_UI.equals(command)) {
            enableTouchExplorationMode();
            Tts.speakAfterQueue("Started touch exploration", ttsWrapper);
            Log.d("service yolo", "starting touch exploration");
            return;
        }
        else if(STOP_EXPLORE_UI.equals(command)) {
            Log.d("service yolo", "stopping touch exploration");
            disableTouchExplorationMode();
            Tts.speakAfterQueue("Stopped touch exploration", ttsWrapper);
            return;
        }
        else if(START_HIGHLIGHT.equals(command)) {
            highlighter.startHighlighting();
            focusRelevantNode(ttsWrapper);
            Tts.speakAfterQueue("Started highlighting nodes", ttsWrapper);
            Log.d("service yolo", "starting highlighting nodes");
            return;
        }
        else if(STOP_HIGHLIGHT.equals(command)) {
            highlighter.stopHighlighting();
            Tts.speakAfterQueue("Stopped highlighting nodes", ttsWrapper);
            Log.d("service yolo", "stopped highlighting nodes");
            return;
        }
        else if(INPUT_TEXT.equals(command)) {
            if(!focusedNode.isEditable()) {
                speakCancellingQueue("not editable " + AccessibilityNodeUtils.getText(focusedNode), ttsWrapper);
                return;
            }
            if(TextUtils.isEmpty(param)) {
                speakCancellingQueue("No Text presented", ttsWrapper);
                return;
            }
            boolean performed = fillText(param, focusedNode);
            if(!performed) Log.e("yolo", "could not edit" + AccessibilityNodeUtils.getText(focusedNode));
            String operationResult = performed ? "Filled text field with " + param : "Could not edit text field";
            Tts.speakAfterQueue(operationResult, ttsWrapper);
        }
        else if (LAUNCH_APP.equals(command)) {
            boolean launched = Android.launchApp(param, this);
            if (launched) {
                Tts.speakAfterQueue("tried launching app", ttsWrapper);
            } else {
                Tts.speakAfterQueue("Cannot launch selected app", ttsWrapper);
            }
        }
        else if (GLOBAL_BACK.equals(command)){
            performGlobalAction(GLOBAL_ACTION_BACK);
            speakAfterQueue("Performed global back", ttsWrapper);
        }
        else if (GLOBAL_HOME.equals(command)){
            performGlobalAction(GLOBAL_ACTION_HOME);
            speakAfterQueue("Performed global home", ttsWrapper);
        }
        else if (GLOBAL_RECENT_APPS.equals(command)){
            performGlobalAction(GLOBAL_ACTION_RECENTS);
//            TODO: GLOBAL_ACTION_KEYCODE_HEADSETHOOK
            speakAfterQueue("Performed global recent apps", ttsWrapper);
        }
        else if(DO_MULTIPLE_TIMES.equals(command)) {
            safeCall(() -> {
                List<String> variables = CommandUtils.getVariables(DO_MULTIPLE_TIMES, commandAndArguments.triggerVoiceInput, this);
                int times = Math.toIntExact(getNumber(variables.get(0)));
                String commandToExecuteMultipleTimes = variables.get(1);
                VoiceAutomationInputService.repeat(commandToExecuteMultipleTimes, times);
            }, e -> {
                Tts.speakAfterQueue("Failed to repeat", ttsWrapper);
                Log.e("service command", "failed to repeat", e);
            });
        }
    }
    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        return safeFunction(this::handleKeyEvent, event, e -> {
            Tts.speakAfterQueue("Failed to handle keyevent", genericTtsWrapper);
            return false;
        });
    }

    private boolean handleKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        if(!isDPADButton(keyCode)) return false;
        if(action == KeyEvent.ACTION_DOWN) return true;
        boolean isLongPress = (event.getFlags() & FLAG_LONG_PRESS) != 0;
        if(isLongPress) Log.d("yolo", "long pressed ");
        // TODO: handle long press
        AccessibilityNodeInfo focusedNode = getFocusedNode();
        if(focusedNode == null) {
            focusFirstFocusableChildOfRoot(genericTtsWrapper);
            return true;
        }
        if (keyCode == KEYCODE_DPAD_RIGHT){
            onUserAction(new CommandAndArguments(NEXT, "", genericTtsWrapper));
            return true;
        }
        else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
            if(event.isShiftPressed()){
                onUserAction(new CommandAndArguments(SCROLL_DOWN, "", genericTtsWrapper));
                return true;
            }
            onUserAction(new CommandAndArguments(IN, "", genericTtsWrapper));
            return true;
        }
        else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT){
            onUserAction(new CommandAndArguments(PREV, "", genericTtsWrapper));
            return true;
        }
        else if (keyCode == KeyEvent.KEYCODE_DPAD_UP){
            if(event.isShiftPressed()){
                onUserAction(new CommandAndArguments(SCROLL_UP, "", genericTtsWrapper));
                return true;
            }
            onUserAction(new CommandAndArguments(OUT, "", genericTtsWrapper));
            return true;
        }
        else if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KEYCODE_ENTER){
            onUserAction(new CommandAndArguments(TAP, "", genericTtsWrapper));
            return true;
        }
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    public AccessibilityNodeInfo getRoot(){
        return getRootInActiveWindow();
    }

    private void focusRelevantNode(Tts.TtsWrapper ttsWrapper) {
        AccessibilityNodeInfo root = getRoot();
        if(root == null) return;
        int windowId = root.getWindowId();
        if(!focusPerWindow.containsKey(windowId)) {
            focusFirstFocusableChildOfRoot(ttsWrapper);
            return;
        }
        AccessibilityNodeInfo focusedNode = focusPerWindow.get(windowId);
        if(isValidNode(focusedNode)) {
            highlightNode(focusedNode, ttsWrapper);
            return;
        }
        focusFirstFocusableChildOfRoot(ttsWrapper);
    }

    private void focusFirstFocusableChildOfRoot(Tts.TtsWrapper ttsWrapper) {
        AccessibilityNodeInfo firstChild = firstFocusableNode(getRoot());
        if(firstChild == null) return;
        highlightNode(firstChild, ttsWrapper);
    }


    private void announceWindowTitleAndFocusRelevantNode(AccessibilityEvent event) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) { // windowInfo.getTitle is available in N+
            return;
        }
        int windowId = event.getWindowId();
        if(currentWindowId == windowId) return;
        List<AccessibilityWindowInfo> windows = getWindows();
        for(AccessibilityWindowInfo windowInfo: windows) {
            if(windowInfo.getId() != windowId) continue;
            if(!windowInfo.isFocused() || !windowInfo.isActive()) continue;
            currentWindowId = windowId;
            speakCancellingQueue(windowInfo.getTitle(), genericTtsWrapper);
            runOnMainDelayed(() -> safeCall(() ->focusRelevantNode(genericTtsWrapper), e -> {
                Log.e("accessibility", "failed announcing window title and relevant node");
            }), 500); // waiting for title announcement to complete
        }
    }

    public void highlightNode(AccessibilityNodeInfo nodeInfo, Tts.TtsWrapper ttsWrapper){
        if(nodeInfo == null) return;
        int windowId = nodeInfo.getWindowId();
        focusPerWindow.put(windowId, nodeInfo);
        updateHighlightAreaOnScreen(nodeInfo);
        speakCancellingQueue(AccessibilityNodeUtils.getText(nodeInfo), ttsWrapper);
        speakAfterQueue(nodeMetaText(nodeInfo), ttsWrapper);
    }

    private void updateHighlightAreaOnScreen(AccessibilityNodeInfo nodeInfo) {
        nodeInfo.getBoundsInScreen(viewBordersRect);
        highlighter.highlight(viewBordersRect);
    }


    @Nullable
    public AccessibilityNodeInfo getFocusedNode(){
        AccessibilityNodeInfo root = getRoot();
        if(root == null) return null;
        int windowId = root.getWindowId();
        if(!focusPerWindow.containsKey(windowId)) return null;
        AccessibilityNodeInfo focusedNode = focusPerWindow.get(windowId);
        if(!isValidNode(focusedNode)) return null;
        return focusedNode;

    }
    @Override
    public boolean onGesture(@NonNull AccessibilityGestureEvent gestureEvent) {
//        return true;
        return super.onGesture(gestureEvent);
    }

    private void onCommand(CommandAndArguments commandAndArguments){
        Log.d("yolo action", commandAndArguments.command.toString());
        onUserAction(commandAndArguments);
    }

    private void onNotification(StatusBarNotification notification) {
//        runAsync(() -> handleNotification(notification, getTtsForSleep(genericTtsWrapper), this));
    }
    @Override
    public void onServiceConnected() {
        safeCall(this::onServiceConnect, e -> {
            Tts.speakAfterQueue("Failed to start service", genericTtsWrapper);
        });
    }

    private void onServiceConnect() {
        acquireScreenWakeLock(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            requestAudioFocusAndMonitorIfUserPlaysSomeMedia(this);
        }
        hideSoftKeyboard();
        MessageBus.subscribe(ENTERING_SLEEP_MODE, onEnteringSleepMode);
        MessageBus.subscribe(EXITING_SLEEP_MODE, onExitingSleepMode);
        MessageBus.subscribe(APP_ACTION_ERROR, onAppActionError);
        MessageBus.subscribe(SHOULD_ENTER_SLEEP_MODE, onShouldEnterSleepMode);
        tts = new TextToSpeech(this, status -> {
            if(status == TextToSpeech.ERROR) return;
            tts.setLanguage(Locale.getDefault());
            Tts.speakAfterQueue("Audio initialized", genericTtsWrapper);
        });
        genericTtsWrapper = genericTts(tts);
        if(voiceInputService == null)
            runAsync(() -> voiceInputService = new VoiceInputService(this, () -> voiceInputService.startRecognizing(this, this::onCommand), tts));
        highlighter = new Highlighter(this);
        clearYetToHandleNotifications();
        NotificationsListenerService.registerListener(this::onNotification);
        Log.d("yolo", "accessibility service connected");
        // Set the type of events that this service wants to listen to. Others
        // aren't passed to this service.
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = TYPES_ALL_MASK;
        // TYPE_WINDOWS_CHANGED, TYPE_WINDOW_CONTENT_CHANGED,

        // Set the type of feedback your service provides.
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_SPOKEN;
        info.flags |= AccessibilityServiceInfo.CAPABILITY_CAN_RETRIEVE_WINDOW_CONTENT
                | AccessibilityServiceInfo.DEFAULT
                | AccessibilityServiceInfo.FLAG_REPORT_VIEW_IDS
                | AccessibilityServiceInfo.CAPABILITY_CAN_REQUEST_TOUCH_EXPLORATION
                | AccessibilityServiceInfo.FLAG_REQUEST_FILTER_KEY_EVENTS
                | AccessibilityServiceInfo.FLAG_RETRIEVE_INTERACTIVE_WINDOWS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            info.flags |= AccessibilityServiceInfo.CAPABILITY_CAN_PERFORM_GESTURES; // TODO: can be used for automation
        }


        info.notificationTimeout = 100;

        this.setServiceInfo(info);
    }


    @Override
    public void onInterrupt() {
        Log.d("yolo", "service interrupted");
        safeCall(this::shutdownOurServiceAndResources, e -> {
            Tts.speakAfterQueue("Failed to shutdown service", genericTtsWrapper);
        });
    }

    private void shutdownOurServiceAndResources() {
        if(tts !=null) tts.shutdown();
        if(highlighter != null) highlighter.removeHighlight();
        removeScreenWakeLock();
//        removeAudioRecordWakeLock();
        if(voiceInputService != null) voiceInputService.stopRecognizing();
        ConversationProcessor.cleanup();
        voiceInputService = null;
        currentWindowId = -1;
        NotificationsListenerService.deregisterListener(this::onNotification);
        showSoftKeyboard();
        MessageBus.unsubscribe(ENTERING_SLEEP_MODE, onEnteringSleepMode);
        MessageBus.unsubscribe(EXITING_SLEEP_MODE, onExitingSleepMode);
        MessageBus.unsubscribe(APP_ACTION_ERROR, onAppActionError);
        MessageBus.unsubscribe(SHOULD_ENTER_SLEEP_MODE, onShouldEnterSleepMode);
        clearYetToHandleNotifications();
        focusPerWindow.clear();
    }



    private void showSoftKeyboard() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Log.d("yolo keyboard", "enabling");
            getSoftKeyboardController().setShowMode(SHOW_MODE_AUTO);
        }
    }

    private void hideSoftKeyboard() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Log.d("yolo keyboard", "disabling");
            getSoftKeyboardController().setShowMode(SHOW_MODE_HIDDEN);
        }
    }

    @Override
    public void onDestroy() {
        Log.d("yolo", "service destroyed");
        safeCall(this::shutdownOurServiceAndResources, e -> {
            Tts.speakAfterQueue("Failed to shutdown service", genericTtsWrapper);
        });
        super.onDestroy();
    }

}
