package com.example.mia.activities;


import static com.example.mia.appactions.AppAction.ActionType.NAANS;
import static com.example.mia.appactions.AppAction.ActionType.VOICE;
import static com.example.mia.utils.Android.showToastError;
import static com.example.mia.utils.Java.safeCall;
import static com.example.mia.utils.SharedPrefs.saveActions;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.example.mia.R;
import com.example.mia.appactions.AppAction;
import com.example.mia.appactions.AppAction.ActionType;
import com.github.underscore.U;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

public class AddAppActionsActivity extends AppBaseActivity{
    public static final String APP_IDENTIFIER = "APP_IDENTIFIER";
    public static final String APP_NAME = "APP_NAME";
    public static final String PACKAGE_NAME = "PACKAGE_NAME";

    public static final String ERROR_MESSAGE = "ERROR_MESSAGE";

    public static final String ACTION_TRIGGER_VARIABLES = "ACTION_TRIGGER_VARIABLES";
    public static final String ACTION_IDENTIFIER = "ACTION_IDENTIFIER";
    public static final String ACTION_TYPE = "ACTION_TYPE";
    public static final String ACTION_ANNOUNCE_EXTRA = "ACTION_ANNOUNCE";
    public static final String ACTION_VOICE_INPUT = "ACTION_VOICE_INPUT";
    public static final String ACTION_DESCRIPTION = "ACTION_DESCRIPTION";
    public static final String ACTION_DISPLAY_NAME = "ACTION_DISPLAY_NAME";
    public static final String ACTION_TRIGGER_INPUT = "ACTION_TRIGGER_INPUT";
    public static final String REGISTER_ACTIONS_INTENT_ACTION = "com.example.mia.action.REGISTER";
    public static final String ACTION_BUNDLES = "ACTION_BUNDLES";

    public static final String ANNOUNCE_ERROR_ACTION = "ANNOUNCE_ERROR";
    public static final String CHOOSE_ACTION = "MIA_CHOOSE";
    public static final String CHOICE_INDEX_EXTRA = "CHOICE_INDEX";
    public static final String CHOICES_EXTRA = "CHOICES";

    private String appName;
    private String packageName;
    private String appIdentifier;
    private AppCompatTextView disclaimer;
    private LinearLayout actionsContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        appName = intent.getStringExtra(APP_NAME);
        packageName = intent.getStringExtra(PACKAGE_NAME);
        appIdentifier = intent.getStringExtra(APP_IDENTIFIER);
        if(appName == null || packageName == null || appIdentifier == null) {
            Toast.makeText(this, "App details not provided", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
//        if(appIdentifier != getAppIdentifier(packageName)) {
//            confirm('this looks fishy, either some other app is trying to impersonate or app has been reinstalled');
//            return;
//        }
        disclaimer = findViewById(R.id.app_action_add_disclaimer);
        disclaimer.setText(getString(R.string.app_action_add_edit_disclaimer, appName));
        actionsContainer = findViewById(R.id.actions_container);
        showActions(intent);
    }

    private void showActions(Intent intent) {
        ArrayList<Bundle> actionBundles = intent.getParcelableArrayListExtra(ACTION_BUNDLES);
        if(actionBundles == null) {
            Toast.makeText(this, "No Actions provided", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        List<AppAction> actions = U.map(actionBundles, this::getAction);
        U.forEach(actions, this::addActionToView);
        findViewById(R.id.save).setOnClickListener(v -> {
            safeCall(() -> {
                saveActions(actions, packageName, this);
                finish();
            },  e -> {
                e.printStackTrace();
                Log.e("actions save", e.getMessage(), e);
                showToastError("Failed saving actions: " + e.getMessage(), this);});
        });
    }

    private void addActionToView(AppAction action) {
        View actionView = getLayoutInflater().inflate(R.layout.app_action, null);
        ((AppCompatTextView)actionView.findViewById(R.id.name)).setText(action.displayName);
        ((AppCompatTextView)actionView.findViewById(R.id.description)).setText(action.description);
        if(VOICE.equals(action.actionType))
            ((TextInputEditText)actionView.findViewById(R.id.command_access_text)).setText(action.defaultVoiceInput);
        else if(NAANS.equals(action.actionType))
            ((TextInputEditText)actionView.findViewById(R.id.command_access_text)).setText(action.announceText);
        actionsContainer.addView(actionView);
    }

    private AppAction getAction(Bundle actionBundle) {
        return new AppAction(appName,
                packageName,
                appIdentifier,
                actionBundle.getString(ACTION_DISPLAY_NAME),
                actionBundle.getString(ACTION_DESCRIPTION),
                actionBundle.getString(ACTION_VOICE_INPUT),
                actionBundle.getString(ACTION_IDENTIFIER),
                ActionType.valueOf(actionBundle.getString(ACTION_TYPE, "VOICE")),
                actionBundle.getString(ACTION_ANNOUNCE_EXTRA)
        );

    }

    @Override
    int getLayoutResource() {
        return R.layout.activity_app_actions;
    }

    @Override
    int title() {
        return R.string.title;
    }
}
