package com.example.mia.activities;

import static android.Manifest.permission.BLUETOOTH;
import static android.Manifest.permission.BLUETOOTH_CONNECT;
import static android.Manifest.permission.BLUETOOTH_SCAN;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS;
import static android.text.TextUtils.isEmpty;

import static com.example.mia.Command.getTextFor;
import static com.example.mia.utils.Android.showToastError;

import static java.util.Arrays.asList;

import android.Manifest;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.AppCompatToggleButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.mia.Command;
import com.example.mia.NotificationsListenerService;
import com.example.mia.R;
import com.example.mia.voice.DefaultVoiceRecognitionService;
import com.example.mia.voice.VoiceRecognitionService;
import com.github.underscore.U;

import java.util.List;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1234;
    private static final int PERMISSIONS_REQUEST_PHONE_STATE = 2345;
    private TextView textView;
    private AppCompatToggleButton listenButton;
    private TextToSpeech tts;
    private VoiceRecognitionService voiceRecognitionService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        askNotificationAccessIfNotPresent();
        askForPhoneStatePermissionIfNotGranted();
        askForBluetoothPermissionIfNotGranted();
        askForIgnoringPowerOptimizations();
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.speech_to_text_result);
        listenButton = findViewById(R.id.listen_toggle_button);
        listenButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked) {
               if(voiceRecognitionService ==null) return;
               voiceRecognitionService.resume();
            }
            else {
                if(voiceRecognitionService ==null) return;
                voiceRecognitionService.pause();
            }
        });
        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PERMISSION_GRANTED) {
            Log.d("yolo", "asking permissions: ");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
        } else {
            createVoiceRecognitionInstance();
        }
        showVoiceCommands();
        findViewById(R.id.edit_workflows).setOnClickListener(v -> {
            startActivity(new Intent(this, WorkflowsActivity.class));
        });
        ttsHindiCheck();
    }

    private void askForIgnoringPowerOptimizations() {
        PowerManager powerManager = getSystemService(PowerManager.class);
        if(powerManager.isIgnoringBatteryOptimizations(getPackageName())) return;
        startActivity(new Intent(ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,  Uri.parse("package:"+getPackageName())));
    }

    private void askForPhoneStatePermissionIfNotGranted() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) == PERMISSION_GRANTED) return;
        Log.d("yolo", "asking permissions for phone:  ");
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_PHONE_STATE);
    }

    private void askForBluetoothPermissionIfNotGranted() {
        List<String> permissions = Build.VERSION.SDK_INT >=31 ? asList(BLUETOOTH, BLUETOOTH_CONNECT, BLUETOOTH_SCAN) : asList(BLUETOOTH);
        boolean granted = U.all(permissions, permission -> ContextCompat.checkSelfPermission(this, permission) == PERMISSION_GRANTED);
        if(granted) return;
        Log.d("yolo", "asking permissions for bluetooth: ");
        ActivityCompat.requestPermissions(this, permissions.toArray(new String[]{}), PERMISSIONS_REQUEST_PHONE_STATE);
    }


    private void createVoiceRecognitionInstance() {
        voiceRecognitionService = new DefaultVoiceRecognitionService(this, () -> listenButton.setEnabled(true), e -> showToastError(e.getMessage(), this), input -> {
            textView.setText(input);
        });
    }

    private void ttsHindiCheck() {
        tts = new TextToSpeech(this, status -> {
            if (status == TextToSpeech.ERROR) {
                System.out.println("tts errored initialization yolo");
                return;
            }
            tts.setLanguage(new Locale("ur"));
            Set<Locale> availableLanguages = tts.getAvailableLanguages();
            for (Locale locale : availableLanguages) {
                System.out.println("lang: " + locale.getLanguage() + " lang: " + locale.getDisplayLanguage());
                System.out.println("country code: " + locale.getCountry() + " country: " + locale.getDisplayCountry());
            }
            tts.speak("5:10", TextToSpeech.QUEUE_ADD, null ,"1");
//            tts.speak("Rahil", TextToSpeech.QUEUE_ADD, null ,"1");
//            tts.speak("Sulaiman", TextToSpeech.QUEUE_ADD, null ,"1");
        });

    }

    private void askNotificationAccessIfNotPresent() {
        if(Build.VERSION.SDK_INT < 30) return;
        ComponentName componentName = new ComponentName(this, NotificationsListenerService.class.getName());
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager.isNotificationListenerAccessGranted(componentName)) return;
        Intent notificationAccessSettings = new Intent(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_DETAIL_SETTINGS));

        notificationAccessSettings.putExtra(
                Settings.EXTRA_NOTIFICATION_LISTENER_COMPONENT_NAME,
                componentName.flattenToString()
        );
        startActivity(notificationAccessSettings);
    }

    private void showVoiceCommands() {
        StringBuffer commandsText = new StringBuffer();
        for(Command command : Command.class.getEnumConstants()) {
            String text = getTextFor(command, this);
            if(isEmpty(text)) continue;
            commandsText.append(String.format("%s: %s\n",command, text));
        }
        ((AppCompatTextView)findViewById(R.id.voice_controls_commands_list)).setText(commandsText.toString());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                // Recognizer initialization is a time-consuming and it involves IO,
                // so we execute it in async task
                createVoiceRecognitionInstance();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (voiceRecognitionService == null) return;
        voiceRecognitionService.pause();
        voiceRecognitionService.shutdown();
    }

}
