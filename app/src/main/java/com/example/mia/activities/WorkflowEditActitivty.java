package com.example.mia.activities;

import static android.text.TextUtils.isEmpty;
import static com.example.mia.Command.PROCESS_VOICE_INPUT;
import static com.example.mia.Command.commandNameToCommandMapping;
import static com.example.mia.Command.textInputTypes;
import static com.example.mia.utils.Android.showToastError;
import static com.example.mia.utils.Domain.WORKFLOW_TITLE_STRING_EXTRA;
import static com.example.mia.workflows.Action.allowedDelays;
import static com.example.mia.workflows.WorkflowStore.addWorkflow;
import static com.example.mia.workflows.WorkflowStore.getWorkflow;
import static com.example.mia.workflows.WorkflowStore.updateWorkflow;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatSpinner;

import com.example.mia.Command;
import com.example.mia.Exceptions;
import com.example.mia.Exceptions.WorkflowAlreadyExists;
import com.example.mia.Exceptions.WorkflowNotFound;
import com.example.mia.R;
import com.example.mia.utils.Android;
import com.example.mia.workflows.Action;
import com.example.mia.workflows.Action.DynamicInput;
import com.example.mia.workflows.Workflow;
import com.github.underscore.U;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

public class WorkflowEditActitivty extends AppBaseActivity {

    private Workflow workflow;
    private LinearLayout workflowItemsHolder;
    private TextInputEditText titleEditText;
    private boolean creatingNewWorkflow;
    private String originalWorkflowTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        originalWorkflowTitle = getIntent().getStringExtra(WORKFLOW_TITLE_STRING_EXTRA);
        creatingNewWorkflow = originalWorkflowTitle == null;
        titleEditText = findViewById(R.id.workflow_title);
        workflowItemsHolder = findViewById(R.id.workflow_items);
        findViewById(R.id.add_command).setOnClickListener(v -> addStep(new Action(PROCESS_VOICE_INPUT, "")));
        if (!creatingNewWorkflow) loadWorkflow(originalWorkflowTitle);
    }

    private void loadWorkflow(String workflowTitle) {
        workflow = getWorkflow(workflowTitle, this);
        if (workflow == null) return;
        getSupportActionBar().setTitle(workflow.title);
        titleEditText.setText(workflow.title);
        drawStepsInWorkflow();
    }

    private void drawStepsInWorkflow() {
        U.forEach(workflow.actions, this::addStep);
    }

    private void addStep(Action action) {
       workflowItemsHolder.addView(createStepView(action));
    }

    private View createStepView(Action action) {
        View inflatedView = getLayoutInflater().inflate(R.layout.listitem_command, null);

        AppCompatSpinner commandsDropDown = inflatedView.findViewById(R.id.command_name);
        ArrayAdapter<String> commandsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        commandsAdapter.addAll(U.sort(commandNameToCommandMapping.keySet()));
        commandsDropDown.setAdapter(commandsAdapter);

        TextInputEditText paramEditText = inflatedView.findViewById(R.id.param);
        TextInputEditText dynamicQuestionEditText = inflatedView.findViewById(R.id.dynamic_input_question);
        AppCompatSpinner inputTypeDropdown = inflatedView.findViewById(R.id.input_type);
        AppCompatSpinner delayForNextAction = inflatedView.findViewById(R.id.delay_for_next_action);

        ArrayAdapter<String> inputTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        inputTypeAdapter.addAll(U.map(textInputTypes, Command::toString));
        inputTypeDropdown.setAdapter(inputTypeAdapter);

        ArrayAdapter<String> delayForNextActionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        delayForNextActionAdapter.addAll(U.map(allowedDelays, String::valueOf));
        delayForNextAction.setAdapter(delayForNextActionAdapter);

        commandsDropDown.setSelection(commandsAdapter.getPosition(action.command.toString()));
        paramEditText.setText(action.param);
        delayForNextAction.setSelection(delayForNextActionAdapter.getPosition(String.valueOf(action.delayForNextActionInMillis)));
        if(action.dynamicInput != null){
            dynamicQuestionEditText.setText(action.dynamicInput.question);
            inputTypeDropdown.setSelection(inputTypeAdapter.getPosition(action.dynamicInput.inputType.toString()));
        }

        inflatedView.findViewById(R.id.remove_command).setOnClickListener(v -> {
            workflowItemsHolder.removeView(inflatedView);
        });

        inflatedView.findViewById(R.id.add_below_command).setOnClickListener(v -> {
            workflowItemsHolder.addView(createStepView(new Action(PROCESS_VOICE_INPUT, "")), workflowItemsHolder.indexOfChild(inflatedView) + 1);
        });

        return inflatedView;
    }
    private Action getAction(View holder){
        String commandName = ((AppCompatSpinner) holder.findViewById(R.id.command_name)).getSelectedItem().toString();
        Command command = commandNameToCommandMapping.get(commandName);
        String param = Android.getText(holder.findViewById(R.id.param));
        String dynamicQuestion = Android.getText(holder.findViewById(R.id.dynamic_input_question));
        int delayForNextAction = Integer.parseInt(((AppCompatSpinner) holder.findViewById(R.id.delay_for_next_action)).getSelectedItem().toString());
        if(isEmpty(dynamicQuestion)){
            return new Action(command, param, delayForNextAction, null);
        }
        String inputTypeAsString = ((AppCompatSpinner) holder.findViewById(R.id.input_type)).getSelectedItem().toString();
        Command inputType = commandNameToCommandMapping.get(inputTypeAsString);
        return new Action(command, param, delayForNextAction, new DynamicInput(dynamicQuestion, inputType));
    }
    private void createWorkflowFromUIAndSave(){
        List<Action> actions = U.chain(U.range(workflowItemsHolder.getChildCount()))
                .map(workflowItemsHolder::getChildAt)
                .map(this::getAction)
                .value();

        String title = Android.getText(titleEditText);
        if(isEmpty(title)){
            Toast.makeText(this, R.string.please_enter_title, Toast.LENGTH_LONG).show();
            return;
        }
        if(actions.isEmpty()){
            Toast.makeText(this, R.string.no_commands_to_save, Toast.LENGTH_LONG).show();
            return;
        }
        actions.get(actions.size() - 1).shouldDisableTts = false;
        if (saveWorkflow(title, actions)) return;
        finish();
    }

    private boolean saveWorkflow(String title, List<Action> actions) {
        if(creatingNewWorkflow) {
            try {
                addWorkflow(new Workflow(title, actions), this);
            } catch (WorkflowAlreadyExists e) {
                showToastError("workflow with this name already exists", this);
                return true;
            }
        }
        else {
            try {
                updateWorkflow(new Workflow(title, actions), originalWorkflowTitle, this);
            } catch (WorkflowNotFound e) {
                showToastError("No workflow found with this name", this); // shouldn't be happening at all in general
                return true;
            } catch (WorkflowAlreadyExists e) {
                showToastError("workflow with this name already exists", this);
                return true;
            }
        }
        return false;
    }

    @Override
    int getLayoutResource() {
        return R.layout.activity_workflow_edit_actitivty;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.save)
                .setIcon(R.drawable.ic_save_black_24dp)
                .setOnMenuItemClickListener(item -> {
                    createWorkflowFromUIAndSave();
                    return true;
                })
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    int title() {
        return R.string.new_workflow;
    }

}