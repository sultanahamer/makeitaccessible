package com.example.mia.activities;

import static com.example.mia.utils.Domain.getIntentToCreateNewWorkflow;
import static com.example.mia.utils.Domain.getIntentToEditWorkflow;
import static com.example.mia.workflows.WorkflowStore.getAllWorkflows;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;

import com.example.mia.R;
import com.example.mia.StoreChangeListener;
import com.example.mia.VoiceAutomationInputService;
import com.example.mia.workflows.Workflow;
import com.example.mia.workflows.WorkflowStore;

import java.util.List;

public class WorkflowsActivity extends AppBaseActivity {

    private List<Workflow> allWorkflows;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        populateWorkflows();
    }


    private void populateWorkflows() {
        ListView workflowsListView = findViewById(R.id.workflows);
        allWorkflows = getAllWorkflows(this);
        workflowsListView.setOnItemClickListener((adapterView, view, index, l) -> {
            startActivity(getIntentToEditWorkflow(allWorkflows.get(index), this));
        });
        workflowsListView.setOnItemLongClickListener((adapterView, view, index, l) -> {
            VoiceAutomationInputService.run(allWorkflows.get(index));
            return true;
        });
        ArrayAdapter<Workflow> workflowsListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, allWorkflows);
        workflowsListView.setAdapter(workflowsListAdapter);
        WorkflowStore.addListener(new StoreChangeListener<Workflow>() {
            @Override
            public void refresh(List<Workflow> items) {
                workflowsListAdapter.clear();
                workflowsListAdapter.addAll(items);
                workflowsListAdapter.notifyDataSetChanged();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.add_new_workflow)
                .setIcon(R.drawable.baseline_add_circle_outline_24)
                .setOnMenuItemClickListener(item -> {
                    startActivity(getIntentToCreateNewWorkflow(this));
                    return true;
                })
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    int getLayoutResource() {
        return R.layout.activity_workflows;
    }

    @Override
    int title() {
        return R.string.workflows;
    }
}
