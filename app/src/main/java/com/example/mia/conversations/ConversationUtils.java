package com.example.mia.conversations;

import static com.example.mia.Command.CANCEL;
import static com.example.mia.conversations.ConversationProcessor.startConversation;

import android.content.Context;

import androidx.core.util.Consumer;

import com.example.mia.Command;
import com.example.mia.utils.Tts;

public class ConversationUtils {
    public static Runnable ignore = () -> {};
    public static void uponSuccessfulTextCapture(Consumer<String> doThis, Command inputType, Tts.TtsWrapper ttsWrapper, Context context){
        withTextCapture(doThis, ignore, inputType, ttsWrapper, context);
    }

    public static void withTextCapture(Consumer<String> doThis, Runnable onCancel, Command inputType, Tts.TtsWrapper ttsWrapper, Context context){
        startConversation(new TextInputConversation(inputType, ttsWrapper, context), commandStringPair -> {
            if(commandStringPair.command == CANCEL) onCancel.run();
            else doThis.accept(commandStringPair.param);
        });
    }
}
