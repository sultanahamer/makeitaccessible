package com.example.mia.conversations.naans;

import static com.example.mia.Command.CANCEL;
import static com.example.mia.Command.getTextFor;
import static com.example.mia.conversations.ConversationProcessor.cancelCurrentConversation;
import static com.example.mia.conversations.ConversationProcessor.endCurrentConversation;
import static com.example.mia.conversations.ConversationProcessor.startConversation;
import static com.example.mia.utils.Async.runAsync;

import androidx.core.util.Consumer;
import androidx.core.util.Pair;

import android.content.Context;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import com.example.mia.Command;
import com.example.mia.R;
import com.example.mia.utils.ListUtils;
import com.example.mia.utils.Tts;
import com.github.underscore.Function;
import com.github.underscore.U;

import java.util.List;

public class NaansChoice extends NaansConversation {
    private final List<String> choices;
    private final String question;
    private final String cancelCommand;
    private final Context context;
    public static int REPEAT_OPTION_GAP = 3000;
    public static int MAX_WAIT_TIME = 10000;
    public static int REPEAT_TIMES = 2;
    private int choiceIndexBeingAnnounced = 0;
    private boolean choosen = false;
    private String nextChoiceText;
    private String currentUtteranceId = "";
    private long waitTill = 0;

    public NaansChoice(List<String> choices, Tts.TtsWrapper ttsWrapper, Context context) {
        this(choices, "choose one of", ttsWrapper, context);
    }

    // param question will be replacing default text of `choose one of`
    public NaansChoice(List<String> choices, String question, Tts.TtsWrapper ttsWrapper, Context context) {
        super(ttsWrapper);
        this.choices = choices;
        this.question = question;
        cancelCommand = getTextFor(CANCEL, context);
        this.context = context;
        nextChoiceText = context.getString(R.string.next_choice);
        ttsWrapper.tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {

            @Override
            public void onStop(String utteranceId, boolean interrupted) {
                Log.d("naans choice", "uttering stopped" + utteranceId);
                if(currentUtteranceId.equals(utteranceId)) waitTill = 0;
            }

            @Override
            public void onStart(String utteranceId) {
                Log.d("naans choice", "uttering " + utteranceId);
            }

            @Override
            public void onDone(String utteranceId) {
                Log.d("naans choice", "uttering done " + utteranceId);
                if(currentUtteranceId.equals(utteranceId)) waitTill = System.currentTimeMillis() + REPEAT_OPTION_GAP;
            }

            @Override
            public void onError(String utteranceId) {
                Log.d("naans choice", "uttering error " + utteranceId);
                if(currentUtteranceId.equals(utteranceId)) waitTill = 0;
            }
        });
    }

    @Override
    public void processNextInput(String input) {
        int choiceIndex = choiceIndexBeingAnnounced;
        this.choosen = true;
        endCurrentConversation(new Pair<>(Command.CHOOSE, choices.get(choiceIndex)));
    }

    @Override
    public void start() {
        runAsync(this::repeat);
    }

    void repeat() {
        Tts.speakAfterQueue(question, ttsWrapper);
        for (int i = 0; i < choices.size(); i++) {
            if (choosen || stop) return;
            String choice = choices.get(i);
            currentUtteranceId = "current choice " + choice;
            Tts.speakAfterQueue(choice, currentUtteranceId, ttsWrapper);
            if (choosen || stop) return;
            choiceIndexBeingAnnounced = i;
            waitForInput();
            if (choosen || stop) return;
            Tts.speakAfterQueue(choice, currentUtteranceId, ttsWrapper);
            waitForInput();
        }
        Tts.speakAfterQueue("Closing", ttsWrapper);
        cancelCurrentConversation();
    }

    private void waitForInput() {
        waitTill = System.currentTimeMillis() + MAX_WAIT_TIME;
        try {
            while (true) {
                if(waitTill > System.currentTimeMillis()) Thread.sleep(100);
                else return;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //    public static Choice confirmConversation(String question, Tts.TtsWrapper ttsWrapper, Context context){
//        String noCommand = getTextFor(NO, context);
//        String confirmCommand = getTextFor(CONFIRM, context);
//        return new Choice(asList(confirmCommand, noCommand), question, ttsWrapper, context);
//    }
//
//    public static void confirmConversation(String question, Runnable onYes, Runnable onNo, Runnable onCancel, Tts.TtsWrapper ttsWrapper, Context context){
//        ConversationProcessor.startConversation(confirmConversation(question, ttsWrapper,context), confirmationResponse -> {
//            if(confirmationResponse.command == CANCEL) onCancel.run();
//            else if(Objects.equals(confirmationResponse.param, getTextFor(NO, context))) onNo.run();
//            else onYes.run();
//        });
//    }
    public static <T> void uponChoosing(List<T> choices, Function<T, String> announcer, Consumer<T> onChoosing, Runnable onCancel, Tts.TtsWrapper ttsWrapper, Context context) {
        startConversation(new NaansChoice(U.map(choices, announcer), ttsWrapper, context), commandAndArguments -> {
            if (commandAndArguments.command == CANCEL) {
                onCancel.run();
                return;
            }
            onChoosing.accept(ListUtils.find(choices, it -> commandAndArguments.param.equalsIgnoreCase(announcer.apply(it))));
        });
    }

    public static void uponChoosing(List<String> choices, Consumer<String> onChoosing, Runnable onCancel, Tts.TtsWrapper ttsWrapper, Context context) {
        startConversation(new NaansChoice(choices, ttsWrapper, context), commandAndArguments -> {
            if (commandAndArguments.command == CANCEL) {
                onCancel.run();
                return;
            }
            onChoosing.accept(ListUtils.find(choices, commandAndArguments.param::equals));
        });
    }
}
