package com.example.mia.conversations;

import static com.example.mia.Command.CANCEL;
import static com.example.mia.Command.CHOOSE;
import static com.example.mia.Command.CONFIRM;
import static com.example.mia.Command.NO;
import static com.example.mia.Command.getTextFor;
import static com.example.mia.conversations.ConversationProcessor.endCurrentConversation;
import static com.example.mia.utils.Java.getMatchingIgnoringCase;
import static java.util.Arrays.asList;

import android.content.Context;

import androidx.core.util.Pair;

import com.example.mia.utils.Tts;

import java.util.List;
import java.util.Objects;

public class Choice extends Conversation {
    private final List<String> choices;
    private final String question;
    private final String cancelCommand;

    public Choice(List<String> choices, Tts.TtsWrapper ttsWrapper, Context context) {
        this(choices, "choose one of", ttsWrapper, context);
    }
// param question will be replacing default text of `choose one of`
    public Choice(List<String> choices, String question, Tts.TtsWrapper ttsWrapper, Context context) {
        super(ttsWrapper);
        this.choices = choices;
        this.question = question;
        cancelCommand = getTextFor(CANCEL, context);
    }

    @Override
    public void processNextInput(String input) {
        if(cancelCommand.equals(input)) {
            Tts.speakAfterQueue("cancelling", ttsWrapper);
            endCurrentConversation(new Pair<>(CANCEL, input));
            return;
        }
        String currentChoice = getMatchingIgnoringCase(input, choices);
        if(currentChoice != null) {
            endCurrentConversation(new Pair<>(CHOOSE, currentChoice));
            return;
        }
        Tts.speakAfterQueue(input + " didn't match with the choices presented", ttsWrapper);
        repeat();
    }

    @Override
    public void start() {
        repeat();
    }

    void repeat() {
        Tts.speakAfterQueue(question, ttsWrapper);
        for(String choice : choices) {
            Tts.speakAfterQueue(choice, ttsWrapper);
        }
        Tts.speakAfterQueue("or say " + cancelCommand, ttsWrapper);
    }
    public static Choice confirmConversation(String question, Tts.TtsWrapper ttsWrapper, Context context){
        String noCommand = getTextFor(NO, context);
        String confirmCommand = getTextFor(CONFIRM, context);
        return new Choice(asList(confirmCommand, noCommand), question, ttsWrapper, context);
    }
    public static void confirmConversation(String question, Runnable onYes, Runnable onNo, Runnable onCancel, Tts.TtsWrapper ttsWrapper, Context context){
        ConversationProcessor.startConversation(confirmConversation(question, ttsWrapper,context), confirmationResponse -> {
            if(confirmationResponse.command == CANCEL) onCancel.run();
            else if(Objects.equals(confirmationResponse.param, getTextFor(NO, context))) onNo.run();
            else onYes.run();
        });
    }
}
