package com.example.mia.conversations;

import static com.example.mia.Command.CANCEL;
import static com.example.mia.Command.CHOOSE;
import static com.example.mia.Command.NEXT;
import static com.example.mia.Command.PREV;
import static com.example.mia.Command.getTextFor;
import static com.example.mia.conversations.ConversationProcessor.endCurrentConversation;
import static com.example.mia.utils.AccessibilityNodeUtils.getAllNodesThatSatisfy;
import static com.example.mia.utils.AccessibilityNodeUtils.getText;
import static com.example.mia.utils.AccessibilityNodeUtils.nodeMetaText;
import static java.lang.Math.max;
import static java.util.Arrays.asList;

import android.content.Context;
import android.view.accessibility.AccessibilityNodeInfo;

import androidx.core.util.Pair;
import androidx.core.util.Predicate;

import com.example.mia.MyAccessibilityService;
import com.example.mia.utils.AccessibilityNodeUtils;
import com.example.mia.utils.Tts;
import com.github.underscore.U;

import java.util.List;

public class ChooseNode extends Conversation {
    public static final int NUMBER_OF_NODES_TO_READ_AT_A_TIME = 4;
    private final AccessibilityNodeInfo currentHighlightedNode;
    private final MyAccessibilityService service;
    private final Context context;
    private final String previousCommandText;
    private final String nextCommandText;
    private List<AccessibilityNodeInfo> nodes;
    private int currentStartIndex;

    public ChooseNode(Tts.TtsWrapper ttsWrapper, MyAccessibilityService service, Context context) {
        this(AccessibilityNodeUtils::isHighlightable, ttsWrapper, service, context);
    }

    public ChooseNode(Predicate<AccessibilityNodeInfo> criteriaForNode, Tts.TtsWrapper ttsWrapper, MyAccessibilityService service, Context context) {
        super(ttsWrapper);
        this.currentHighlightedNode = service.getFocusedNode();
        this.service = service;
        this.context = context;
        previousCommandText = getTextFor(PREV, context);
        nextCommandText = getTextFor(NEXT, context);
        Tts.speakAfterQueue("Collecting all nodes, takes a bit", ttsWrapper);
        Predicate<AccessibilityNodeInfo> criteriaAndVisibleToUser = node -> node.isVisibleToUser() && criteriaForNode.test(node);
        nodes = getAllNodesThatSatisfy(criteriaAndVisibleToUser, service.getRoot());
        currentStartIndex = nodes.indexOf(currentHighlightedNode);
        if(currentStartIndex == -1) currentStartIndex = 0;
    }

    @Override
    public void processNextInput(String input) {

    }

    @Override
    public void start() {
        if(nodes == null || nodes.size() == 0){
            Tts.speakAfterQueue("No nodes available to choose from, cancelling", ttsWrapper);
            endCurrentConversation(new Pair<>(CANCEL, ""));
            return;
        }
        announce();
    }

    private void announce() {
        if(nodes.size() == 0) {
            Tts.speakAfterQueue("No node found matching criteria", ttsWrapper);
            endCurrentConversation(new Pair<>(CANCEL, ""));
            return;
        }
        if(nodes.size() == 1) {
            Tts.speakAfterQueue("Found only one node, selecting it", ttsWrapper);
            service.highlightNode(nodes.get(0), ttsWrapper);
            endCurrentConversation(new Pair<>(CHOOSE, ""));
            return;
        }
        List<String> nextNodesToAnnounce = getNextNodesToAnnounce();
        List<String> nodeDescriptionsIncludingPrevAndNext = U.concat(nextNodesToAnnounce, asList(previousCommandText, nextCommandText));
        ChoiceBasedOnIndex choiceBasedOnIndex = new ChoiceBasedOnIndex(nodeDescriptionsIncludingPrevAndNext, ttsWrapper, context);
        ConversationProcessor.startConversation(choiceBasedOnIndex, commandAndArguments -> {
            if(commandAndArguments.command == CANCEL) {
                endCurrentConversation(new Pair<>(commandAndArguments.command, commandAndArguments.param));
                return;
            }
            String choice = commandAndArguments.param;
            if(choice.equals(previousCommandText)) {
                announcePrevNodes();
            }
            else if(choice.equals(nextCommandText)){
                announceNextNodes();
            }
            else {
                int indexInCurrentChoices = nextNodesToAnnounce.indexOf(choice);
                int indexInAllNodes = currentStartIndex + indexInCurrentChoices;
                Tts.speakAfterQueue("Selecting ", ttsWrapper);
                service.highlightNode(nodes.get(indexInAllNodes), ttsWrapper);
                endCurrentConversation(new Pair<>(CHOOSE, ""));
            }

        });
    }

    private boolean hasNextNodes(){
        return nodes.size() - (currentStartIndex + NUMBER_OF_NODES_TO_READ_AT_A_TIME) > 0;
    }

    private void announceNextNodes() {
        if(!hasNextNodes()){
            Tts.speakAfterQueue("We are already at the end choose or cancel one of the nodes", ttsWrapper);
            announce();
            return;
        }
        currentStartIndex =  currentStartIndex + NUMBER_OF_NODES_TO_READ_AT_A_TIME;
        announce();
    }

    private void announcePrevNodes() {
        if(currentStartIndex == 0){
            Tts.speakAfterQueue("We are already at the beginning", ttsWrapper);
            announce();
            return;
        }
        currentStartIndex =  max(currentStartIndex - NUMBER_OF_NODES_TO_READ_AT_A_TIME -1, 0);
        announce();
    }

    private List<String> getNextNodesToAnnounce() {
        return U.chain(U.concat(nodes))
                .last(nodes.size() - currentStartIndex)
                .first(NUMBER_OF_NODES_TO_READ_AT_A_TIME)
                .map(nodeInfo -> getText(nodeInfo) + nodeMetaText(nodeInfo))
                .value();
    }

}
