package com.example.mia.conversations;

import static com.example.mia.Command.CANCEL;
import static com.example.mia.Command.CHOOSE;
import static com.example.mia.Command.REPEAT;
import static com.example.mia.Command.getTextFor;
import static com.example.mia.conversations.ConversationProcessor.endCurrentConversation;
import static com.example.mia.conversations.ConversationProcessor.startConversation;
import static com.example.mia.utils.Digits.getNumber;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.core.util.Pair;

import com.example.mia.utils.ListUtils;
import com.example.mia.utils.Tts;
import com.github.underscore.Function;
import com.github.underscore.U;

import java.util.List;

public class ChoiceBasedOnIndex extends Conversation {
    private final List<String> choices;
    private final String cancelCommand;
    private final Context context;
    private final String repeatCommand;

    public ChoiceBasedOnIndex(List<String> choices, Tts.TtsWrapper ttsWrapper, @NonNull Context context) {
        super(ttsWrapper);
        this.choices = choices;
        cancelCommand = getTextFor(CANCEL, context);
        repeatCommand = getTextFor(REPEAT, context);
        this.context = context;
    }

    @Override
    public void processNextInput(String input) {
        if(cancelCommand.equals(input)) {
            Tts.speakAfterQueue("cancelling", ttsWrapper);
            endCurrentConversation(new Pair<>(CANCEL, input));
            return;
        }
        else if(repeatCommand.equalsIgnoreCase(input)){
            repeat();
            return;
        }
        Long number = getNumber(input);
        if(number == null) {
            Tts.speakAfterQueue("couldn't find digit in " + input + ", retry", ttsWrapper);
            return;
        }
        long selectedChoiceIndex = number - 1;
        if(selectedChoiceIndex <0 || selectedChoiceIndex >= choices.size()){
            Tts.speakAfterQueue("Please choose with in the specified numbers, retry", ttsWrapper);
            return;
        }
        endCurrentConversation(new Pair<>(CHOOSE, choices.get((int) selectedChoiceIndex)));
    }

    @Override
    public void start() {
        if(choices.size() == 0) {
            endCurrentConversation(new Pair<>(CANCEL, ""));
            return;
        }
        else if(choices.size() == 1) {
            endCurrentConversation(new Pair<>(CHOOSE, choices.get(0)));
            return;
        }
        repeat();
    }
    void repeat() {
        U.eachIndexed(choices, (index, choice) -> {
            Tts.speakAfterQueue("To choose", ttsWrapper);
            Tts.speakAfterQueue(choice, ttsWrapper);
            Tts.speakAfterQueue("say " +(index +1), ttsWrapper);
        });
        Tts.speakAfterQueue("or say " + cancelCommand, ttsWrapper);
    }

    public static <T> void uponChoosing(List<T> choices, Function<T, String> announcer, Consumer<T> onChoosing, Runnable onCancel, Tts.TtsWrapper ttsWrapper, Context context) {
        startConversation(new ChoiceBasedOnIndex(U.map(choices, announcer), ttsWrapper, context), commandAndArguments -> {
            if(commandAndArguments.command == CANCEL) {
                onCancel.run();
                return;
            }
            onChoosing.accept(ListUtils.find(choices, it -> commandAndArguments.param.equalsIgnoreCase(announcer.apply(it))));
        });
    }
}
