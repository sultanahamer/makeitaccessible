package com.example.mia.conversations.naans;

import com.example.mia.conversations.Conversation;
import com.example.mia.utils.Tts;

public abstract class NaansConversation extends Conversation {
    protected boolean stop = false;
    public NaansConversation(Tts.TtsWrapper ttsWrapper) {
        super(ttsWrapper);
    }

    public void stop(){
        this.stop = true;
    }

    public void resume(){
    }

}
