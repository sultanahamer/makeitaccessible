package com.example.mia.conversations;

import static com.example.mia.conversations.Choice.confirmConversation;
import static com.example.mia.conversations.ChoiceBasedOnIndex.uponChoosing;
import static com.example.mia.conversations.ConversationProcessor.cancelCurrentConversationStackUntil;
import static com.example.mia.conversations.ConversationProcessor.endCurrentConversation;
import static com.example.mia.conversations.ConversationProcessor.ignore;
import static com.example.mia.conversations.ConversationProcessor.ignoreResult;
import static com.example.mia.utils.StatusBarNotificationUtils.announceNotification;
import static com.example.mia.utils.StatusBarNotificationUtils.performAction;
import static java.util.Arrays.asList;

import android.content.Context;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.example.mia.R;
import com.example.mia.utils.StatusBarNotificationUtils;
import com.example.mia.utils.Tts;
import com.github.underscore.U;

import java.util.ArrayList;
import java.util.List;

public class NotificationConversation extends Conversation {


    private final StatusBarNotification notification;
    private final Context context;

    private static StatusBarNotification notificationBeingHandled;

    private static List<StatusBarNotification> yetToHandle = new ArrayList<>(3);

    public NotificationConversation(StatusBarNotification notification, Tts.TtsWrapper ttsWrapper, Context context) {
        super(ttsWrapper);
        this.notification = notification;
        this.context = context;
        notificationBeingHandled = notification;
    }


    @Override
    public void processNextInput(String input) {

    }


    public void announceAndCloseConversation() {
        announceNotification(notification, ttsWrapper, context);
        endCurrentNotificationHandling();
    }
    public void handleOngoingNotification() {
        announceNotification(notification, ttsWrapper, context);
        if(notification.getNotification().actions == null || notification.getNotification().actions.length == 0) {
            endCurrentNotificationHandling();
            return;
        };
        uponChoosing(asList(notification.getNotification().actions),
                x -> x.title.toString(),
                action -> {
                    performAction(action, exception -> Tts.speakAfterQueue("Failed performing action " + action.title, ttsWrapper));
                    endCurrentNotificationHandling();
                },
                this::endCurrentNotificationHandling,
                ttsWrapper,
                context);
    }

    public void handleRegularNotification() {
        confirmConversation(context.getString(R.string.should_read_notification_aloud, StatusBarNotificationUtils.getAppName(notification, context)),
                this::announceAndCloseConversation,
                this::endCurrentNotificationHandling,
                this::endCurrentNotificationHandling,
                ttsWrapper,
                context);
    }

    public void endCurrentNotificationHandling() {
        endCurrentConversation(ignoreResult);
        notificationBeingHandled = null;
        int index = U.findIndex(yetToHandle, x -> x.getId() == notification.getId());
        if(index != -1) yetToHandle.remove(index);
        if(yetToHandle.isEmpty()) return;
        startNotificationHandling(yetToHandle.get(0), ttsWrapper, context);
    }
    @Override
    public void start() {
        if(notification.isOngoing()) handleOngoingNotification();
        else handleRegularNotification();
    }

    private static void startNotificationHandling(StatusBarNotification notification, Tts.TtsWrapper ttsWrapper, Context context) {
        ConversationProcessor.startConversation(new NotificationConversation(notification, ttsWrapper, context), ignore);
    }
    public synchronized static void handleNotification(StatusBarNotification newNotification, Tts.TtsWrapper ttsWrapper, Context context) {
        // collecting it here coz while this method is being executed, the announcment of existing notificaton might end or might not, so notifcationBeingHandled can become null at any particular instant
        StatusBarNotification currentOnGoingNotification = notificationBeingHandled;
        if(currentOnGoingNotification == null) {
            startNotificationHandling(newNotification, ttsWrapper, context);
            return;
        }
        else if(currentOnGoingNotification.getId() == newNotification.getId()) {
            Log.d("newNotification handle yolo", "update to existing newNotification");
            cancelCurrentConversationStackUntil(NotificationConversation.class);
            startNotificationHandling(newNotification, ttsWrapper, context);
            return;
        } else if (!currentOnGoingNotification.isOngoing() && newNotification.isOngoing()) {
            Tts.speakCancellingQueue("Important newNotification arrived, will come back to current one after this", ttsWrapper);
            cancelCurrentConversationStackUntil(NotificationConversation.class);
            startNotificationHandling(newNotification, ttsWrapper, context);
            yetToHandle.add(currentOnGoingNotification);
        } else {
            int index = U.findIndex(yetToHandle, x -> x.getId() == newNotification.getId());
            if(index == -1) yetToHandle.add(newNotification);
            else {
                yetToHandle.remove(index);
                yetToHandle.add(index, newNotification);
            }
        }
    }

    @Override
    public void resume() {
        notificationBeingHandled = notification;
        start();
    }

    public static void clearYetToHandleNotifications() {
        yetToHandle.clear();
    }
}
