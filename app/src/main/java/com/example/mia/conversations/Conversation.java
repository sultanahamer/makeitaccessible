package com.example.mia.conversations;

import com.example.mia.utils.Tts;

public abstract class Conversation {
    public final Tts.TtsWrapper ttsWrapper;
    public Conversation(Tts.TtsWrapper ttsWrapper) {
        this.ttsWrapper = ttsWrapper;
    }

    public abstract void processNextInput(String input);
    public abstract void start();

    public void resume(){
        processNextInput("");
    }

}
