package com.example.mia.conversations;

import static com.example.mia.Command.WAKE_UP;
import static com.example.mia.Command.getTextFor;
import static com.example.mia.utils.Async.runOnMainDelayed;
import static com.example.mia.utils.MessageBus.MessageType.ENTERING_SLEEP_MODE;
import static com.example.mia.utils.MessageBus.MessageType.EXITING_SLEEP_MODE;
import static com.example.mia.utils.Tts.cancelAllOnGoingTts;
import static com.example.mia.utils.WakeLockUtils.*;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Path;
import android.os.Build;

import com.example.mia.NaansAccessibilityService;
import com.example.mia.R;
import com.example.mia.utils.MessageBus;
import com.example.mia.utils.Tts;

public class SleepConversation extends Conversation{

    private final boolean enterSilently;
    private final String wakeUpCommand;
    private final AccessibilityService accessibilityService;

    public SleepConversation(boolean enterSilently, Tts.TtsWrapper ttsWrapper, AccessibilityService accessibilityService) {
        super(ttsWrapper);
        this.enterSilently = enterSilently;
        wakeUpCommand = getTextFor(WAKE_UP, accessibilityService);
        this.accessibilityService = accessibilityService;
    }

    @Override
    public void processNextInput(String input) {
        if (!wakeUpCommand.equals(input)) return;

        Tts.speakAfterQueue(accessibilityService.getString(R.string.woke_up), ttsWrapper);
//        removeCPUWakeLock();
        MessageBus.broadcast(EXITING_SLEEP_MODE);
        ConversationProcessor.cancelCurrentConversation();
        runOnMainDelayed(() -> unlock(accessibilityService), 3000);
    }

    public static void unlock(AccessibilityService accessibilityService) {
        KeyguardManager keyguardManager = (KeyguardManager) accessibilityService.getSystemService(Context.KEYGUARD_SERVICE);
        if (!keyguardManager.isKeyguardLocked() || Build.VERSION.SDK_INT < Build.VERSION_CODES.N) return;
        Path swipePath = new Path();

//Simply using MoveTo if I want to only tap at certain coordinates
        swipePath.moveTo(100, 800);
//Also using LineTo to create gestures
        swipePath.lineTo(100, 0);

        GestureDescription.Builder gestureBuilder = new GestureDescription.Builder();

//The third parameter (1000) is the duration of the gesture in milliseconds, if it is a tap then the screen will be tapped and held for that amount of time, if a gesture, then the gesture will be performed over that amount of time
        gestureBuilder.addStroke(new GestureDescription.StrokeDescription(swipePath, 0, 200));

        accessibilityService.dispatchGesture(gestureBuilder.build(), null, null);
    }

    public static void acquireScreenLockTemporarilyAndUnlock(AccessibilityService accessibilityService) {
        acquireTempScreenWakeLock(accessibilityService);
        runOnMainDelayed(() -> {
            unlock(accessibilityService);
            removeTempScreenWakeLock();
        }, 3000);
    }

    @Override
    public void start() {
        if(enterSilently) cancelAllOnGoingTts(ttsWrapper);
        else Tts.speakCancellingQueue(accessibilityService.getString(R.string.going_to_sleep), ttsWrapper);
//        acquireAudioRecordWakeLock(context);
        MessageBus.broadcast(ENTERING_SLEEP_MODE);
    }

    public static boolean isInSleepMode(){
        if (NaansAccessibilityService.serviceConnected) return false;
        return ConversationProcessor.currentConversation() instanceof SleepConversation;
    }

}
