package com.example.mia.conversations;

import static com.example.mia.conversations.ChoiceBasedOnIndex.uponChoosing;
import static com.example.mia.conversations.ConversationProcessor.cancelCurrentConversation;
import static com.example.mia.utils.StatusBarNotificationUtils.announceNotification;
import static com.example.mia.utils.StatusBarNotificationUtils.notificationActions;


import android.content.Context;
import android.service.notification.StatusBarNotification;

import com.example.mia.NotificationsListenerService;
import com.example.mia.utils.StatusBarNotificationUtils;
import com.example.mia.utils.Tts;
import com.github.underscore.Optional;

import java.util.List;

public class StandbyNotificationConversation extends Conversation {
    private final Context context;

    public StandbyNotificationConversation(Tts.TtsWrapper ttsWrapper, Context context) {
        super(ttsWrapper);
        this.context = context;
    }

    @Override
    public void processNextInput(String input) {

    }

    @Override
    public void start() {
        Optional<StatusBarNotification> latestNotification = NotificationsListenerService.getLatestNotification(context);
        if(latestNotification.isEmpty()) {
            Tts.speakAfterQueue("No notifications present", ttsWrapper);
            cancelCurrentConversation();
            return;
        }
        handleNotification(latestNotification.get());
    }

    public void handleNotification(StatusBarNotification notification) {
        announceNotification(notification, ttsWrapper, context);
        List<StatusBarNotificationUtils.NotificationAction> actions = notificationActions(notification, ttsWrapper.tts);
        uponChoosing(actions, action -> action.actionTitle,
                action -> {
                    action.onAction.accept(notification);
                    cancelCurrentConversation();
                },
                ConversationProcessor::cancelCurrentConversation,
                ttsWrapper, context
        );
    }
}
