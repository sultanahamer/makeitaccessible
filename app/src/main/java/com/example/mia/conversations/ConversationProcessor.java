package com.example.mia.conversations;

import static com.example.mia.Command.CANCEL;
import static com.example.mia.Command.CHOOSE_EDITABLE_NODE_CONV;
import static com.example.mia.Command.CHOOSE_NODE_CONV;
import static com.example.mia.Command.CHOOSE_NODE_WITH_TEXT_CONV;
import static com.example.mia.Command.DO_NOTHING;
import static com.example.mia.Command.INPUT_TEXT_TYPE_TEXT;
import static com.example.mia.Command.LAUNCH_APP_CONV;
import static com.example.mia.Command.LAUNCH_WORKFLOW_CONV;
import static com.example.mia.Command.RECENT_NOTIFICATION;
import static com.example.mia.Command.SLEEP_CONV;
import static com.example.mia.Command.START_TEXT_INPUT_CONV;
import static com.example.mia.Command.TAP;
import static com.example.mia.Command.TAP_NODE_WITH_TEXT;
import static com.example.mia.command.CommandUtils.getVariables;
import static com.example.mia.conversations.ChoiceBasedOnIndex.uponChoosing;
import static com.example.mia.conversations.ConversationUtils.uponSuccessfulTextCapture;
import static com.example.mia.conversations.SleepConversation.isInSleepMode;
import static com.example.mia.utils.AccessibilityNodeUtils.getAllNodesThatSatisfy;
import static com.example.mia.utils.AccessibilityNodeUtils.getInputType;
import static com.example.mia.utils.AccessibilityNodeUtils.getText;
import static com.example.mia.utils.AccessibilityNodeUtils.hasText;
import static com.example.mia.utils.AccessibilityNodeUtils.isHighlightable;
import static com.example.mia.utils.Tts.getTtsForSleep;

import android.accessibilityservice.AccessibilityService;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import androidx.annotation.Nullable;
import androidx.core.util.Pair;

import com.example.mia.CommandAndArguments;
import com.example.mia.Command;
import com.example.mia.MyAccessibilityService;
import com.example.mia.VoiceAutomationInputService;
import com.example.mia.utils.AccessibilityNodeUtils;
import com.example.mia.utils.Tts;
import com.example.mia.workflows.Workflow;
import com.example.mia.workflows.WorkflowStore;
import com.github.underscore.Consumer;
import com.github.underscore.U;

import java.util.List;
import java.util.Stack;

public class ConversationProcessor {
    private static Stack<Pair<Conversation, Consumer<CommandAndArguments>>> conversationsAndResultHandlers = new Stack<>();

    public static void startConversation(Conversation conversation, Consumer<CommandAndArguments> onEnd) {
        conversationsAndResultHandlers.push(new Pair<>(conversation, onEnd));
        conversation.start();
    }

    public static boolean conversationsGoingOn() {
        return !conversationsAndResultHandlers.empty();
    }

    public static void processNextInput(String text) {
        if (!conversationsGoingOn()) return;
        conversationsAndResultHandlers.peek().first.processNextInput(text);
    }

    public static void endCurrentConversation(Pair<Command, String> result) {
        if (!conversationsGoingOn()) return;
        Pair<Conversation, Consumer<CommandAndArguments>> conversationAndResultHandler = conversationsAndResultHandlers.pop();
        conversationAndResultHandler.second.accept(new CommandAndArguments(result.first, result.second, conversationAndResultHandler.first.ttsWrapper));
        if(conversationAndResultHandler.first instanceof NotificationConversation) resumeCurrentConversation();
    }

    public static void resumeCurrentConversation() {
        if (!conversationsGoingOn()) return;
        conversationsAndResultHandlers.peek().first.resume();
    }

    public static void cancelCurrentConversation() {
        endCurrentConversation(new Pair<>(CANCEL, ""));
    }

    public static void cancelCurrentConversationStackUntil(Class<? extends Conversation> conversationType) {
        if(!conversationsGoingOn()) return;
        int conversationFoundAtIndex = U.findIndex(conversationsAndResultHandlers, x -> x.first.getClass().equals(conversationType));
        if(conversationFoundAtIndex == -1) return; // no conversation found with given type
        Pair<Conversation, Consumer<CommandAndArguments>> currentConversation = conversationsAndResultHandlers.pop();
        if(currentConversation.first.getClass().equals(conversationType)) return;
        cancelCurrentConversationStackUntil(conversationType);
    }

    public static Consumer<CommandAndArguments> ignore = ign -> {
    };
    public static Pair<Command, String> ignoreResult = new Pair<>(DO_NOTHING, "");

    public static void cleanup(){
        if(conversationsAndResultHandlers.empty()) return;
        Conversation currentConversation = conversationsAndResultHandlers.peek().first;
        Consumer<CommandAndArguments> currentConversationEndHandler = conversationsAndResultHandlers.peek().second;
        conversationsAndResultHandlers = new Stack<>();
        currentConversationEndHandler.accept(new CommandAndArguments(CANCEL, "", currentConversation.ttsWrapper));
    }

    public static void processConversation(Command conv, String input, Tts.TtsWrapper ttsWrapper, MyAccessibilityService service, Consumer<CommandAndArguments> onResult) {
        if (LAUNCH_APP_CONV.equals(conv)) {
            startConversation(new LaunchApp(ttsWrapper, service), onResult);
        } else if (RECENT_NOTIFICATION.equals(conv)) {
            startConversation(new StandbyNotificationConversation(ttsWrapper, service), ignore);
        } else if (CHOOSE_NODE_CONV.equals(conv)) {
            startConversation(new ChooseNode(ttsWrapper, service, service), ignore);
        } else if (CHOOSE_EDITABLE_NODE_CONV.equals(conv)) {
            startConversation(new ChooseNode(AccessibilityNodeInfo::isEditable, ttsWrapper, service, service), ignore);
        } else if (CHOOSE_NODE_WITH_TEXT_CONV.equals(conv)) {
            uponSuccessfulTextCapture(text -> {
                startConversation(new ChooseNode(node -> isHighlightable(node) && hasText(text, node), ttsWrapper, service, service), ignore);
            }, INPUT_TEXT_TYPE_TEXT, ttsWrapper, service);
        }
        else if(START_TEXT_INPUT_CONV.equals(conv)) {
            AccessibilityNodeInfo focusedNode = service.getFocusedNode();
            if(focusedNode != null && focusedNode.isEditable()) {
                editNode(ttsWrapper, service, onResult, focusedNode);
                return;
            }
            List<AccessibilityNodeInfo> editableNodes = getAllNodesThatSatisfy(node ->
                            node != null && node.isEditable()
                    , service.getRoot());
            if(editableNodes.isEmpty()) {
                Tts.speakAfterQueue("No editable nodes found", ttsWrapper);
                return;
            }
            uponChoosing(editableNodes, node -> getText(node),
                    node -> editNode(ttsWrapper, service, onResult, node),
                    () -> {}, ttsWrapper, service);
        }
        else if(LAUNCH_WORKFLOW_CONV.equals(conv)){
            List<Workflow> allWorkflows = WorkflowStore.getAllWorkflows(service);
            if(allWorkflows.isEmpty()) {
                Tts.speakCancellingQueue("No workflows defined, cancelling", ttsWrapper);
                return;
            }
            uponChoosing(allWorkflows, workflow -> workflow.title,
                    VoiceAutomationInputService::run,
                    () -> Tts.speakCancellingQueue("cancelling", ttsWrapper),
                    ttsWrapper, service);
        }
        else if(SLEEP_CONV.equals(conv)){
            enterSleepMode(false, ttsWrapper, service);
        }
        else if(TAP_NODE_WITH_TEXT == conv) {
            List<String> variables = getVariables(conv, input, service);
            if(variables.isEmpty()) {
                Log.e("conv", "invalid input");
                Tts.speakAfterQueue("invalid input", ttsWrapper);
                return;
            }
            List<AccessibilityNodeInfo> eligibleNodes = getAllNodesThatSatisfy(node ->
                    isHighlightable(node) && node.isClickable() && hasText(variables.get(0), node),
                    service.getRoot());
            if(eligibleNodes.isEmpty()) {
                Tts.speakAfterQueue("no matching nodes found for " + variables.get(0), ttsWrapper);
                return;
            }
            uponChoosing(eligibleNodes, AccessibilityNodeUtils::getText, node -> {
               service.highlightNode(node, ttsWrapper);
               onResult.accept(new CommandAndArguments(TAP, "", ttsWrapper));
            }, () -> {}, ttsWrapper, service);
        }
    }

    private static void editNode(Tts.TtsWrapper ttsWrapper, MyAccessibilityService service, Consumer<CommandAndArguments> onResult, AccessibilityNodeInfo node) {
        service.highlightNode(node, ttsWrapper);
        uponSuccessfulTextCapture(text -> onResult.accept(new CommandAndArguments(Command.INPUT_TEXT, text, ttsWrapper)), getInputType(node), ttsWrapper, service);
    }

    @Nullable
    public static Conversation currentConversation() {
        if(conversationsAndResultHandlers.empty()) return null;
        return conversationsAndResultHandlers.peek().first;
    }

    public static void enterSleepMode(boolean enterSilently, Tts.TtsWrapper ttsWrapper, AccessibilityService service) {
        if(isInSleepMode()) return;
        ConversationProcessor.cleanup();
        startConversation(new SleepConversation(enterSilently, getTtsForSleep(ttsWrapper), service), ignore);
    }

}
