package com.example.mia.conversations;

import static com.example.mia.Command.CANCEL;
import static com.example.mia.Command.CONFIRM;
import static com.example.mia.Command.GLOBAL_BACK;
import static com.example.mia.Command.INPUT_TEXT_TYPE_CHARACTER;
import static com.example.mia.Command.INPUT_TEXT_TYPE_NUMBER;
import static com.example.mia.Command.INPUT_TEXT_TYPE_PUNCTUATION;
import static com.example.mia.Command.INPUT_TEXT_TYPE_TEXT;
import static com.example.mia.Command.NO;
import static com.example.mia.Command.getTextFor;
import static com.example.mia.Command.punctuation;
import static com.example.mia.conversations.Choice.confirmConversation;
import static com.example.mia.utils.Digits.getNumber;
import static com.example.mia.utils.Text.getCharacters;
import static com.example.mia.utils.Text.hasTrailingSpace;
import static com.example.mia.utils.Text.isEmpty;
import static com.example.mia.utils.Text.words;

import android.content.Context;
import android.util.Log;

import androidx.core.util.Pair;

import com.example.mia.Command;
import com.example.mia.utils.Tts;
import com.github.underscore.U;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Stack;

public class TextInputConversation extends Conversation {
    private final String textInputCommandText;
    private final String numberInputCommandText;
    private final String confirmCommandText;
    private final String noCommandText;
    private final String characterInputCommandText;
    private final String cancelCommandText;
    private final String punctuationInputCommandText;
    private Command inputType;
    private final WeakReference<Context> contextRef;
    private String textCapturedSoFar = "";
    private String undo;
    private Stack<Integer> lastEditTextLength = new Stack<>();

    public TextInputConversation(Command inputType, Tts.TtsWrapper ttsWrapper, Context context) {
        super(ttsWrapper);
        this.inputType = inputType;
        contextRef = new WeakReference<>(context);
        textInputCommandText = getTextFor(INPUT_TEXT_TYPE_TEXT, context);
        numberInputCommandText = getTextFor(INPUT_TEXT_TYPE_NUMBER, context);
        characterInputCommandText = getTextFor(INPUT_TEXT_TYPE_CHARACTER, context);
        punctuationInputCommandText = getTextFor(INPUT_TEXT_TYPE_PUNCTUATION, context);
        undo = getTextFor(GLOBAL_BACK, context);
        confirmCommandText = getTextFor(CONFIRM, context);
        noCommandText = getTextFor(NO, context);
        cancelCommandText = getTextFor(CANCEL, context);
    }

    @Override
    public void start() {
        Tts.speakAfterQueue("say something to capture " + getTextFor(inputType, contextRef.get()), ttsWrapper);
    }

    private Pair<Command, String> cancelConvParams(){
        return new Pair<>(CANCEL, "");
    }

    @Override
    public void processNextInput(String input) {
        if (isEmpty(input)) {
            Tts.speakAfterQueue("didn't receive any input, speak some", ttsWrapper);
            return;
        } else if (cancelCommandText.equalsIgnoreCase(input)) {
            Tts.speakAfterQueue("cancelling..", ttsWrapper);
            ConversationProcessor.endCurrentConversation(cancelConvParams());
            return;
        } else if(undo.equalsIgnoreCase(input)) {
            Tts.speakAfterQueue("Performing " + undo, ttsWrapper);
            if(!lastEditTextLength.empty()) textCapturedSoFar = textCapturedSoFar.substring(0, lastEditTextLength.pop());
            if(isEmpty(textCapturedSoFar)) Tts.speakAfterQueue("All content ignored, Start speaking", ttsWrapper);
            else {
                Tts.speakAfterQueue("we are back to", ttsWrapper);
                Tts.speakAfterQueue(textCapturedSoFar, ttsWrapper);
                Tts.speakAfterQueue("proceed further", ttsWrapper);
            }
            return;
        } else if (confirmCommandText.equalsIgnoreCase(input)) {
            finalizeInput();
            return;
        }
        if(inputType == INPUT_TEXT_TYPE_TEXT) processTextInput(input);
        else if(inputType == INPUT_TEXT_TYPE_NUMBER) processNumbersInput(input);
        else if(inputType == INPUT_TEXT_TYPE_CHARACTER) processCharacterInput(input);
    }

    private void afterInputCapture(){
        Tts.speakAfterQueue("so far we got ", ttsWrapper);
        Tts.speakAfterQueue(textCapturedSoFar, ttsWrapper);
    }
    private void processCharacterInput(String input) {
        lastEditTextLength.push(textCapturedSoFar.length());
        String characters = getCharacters(input);
        Tts.speakAfterQueue("received characters " + characters.replaceAll("\\B|\\b", " "), ttsWrapper);
        textCapturedSoFar = textCapturedSoFar + characters;
        afterInputCapture();
    }

    private void processNumbersInput(String input) {
        Long number = getNumber(input);
        if (number == null) {
            Tts.speakAfterQueue("couldn't find digit in " + input + ", retry", ttsWrapper);
            return;
        }
        lastEditTextLength.push(textCapturedSoFar.length());
        textCapturedSoFar = textCapturedSoFar + number;
        afterInputCapture();
    }


    private void processTextInput(String input) {
        List<String> words = words(input);
        if (words.size() > 1) {
            if (characterInputCommandText.equals(words.get(0))) {
                processCharacterInput(U.join(U.rest(words)));
                return;
            }
            else if(numberInputCommandText.equals(words.get(0))) {
                processNumbersInput(U.join(U.rest(words)));
                return;
            }
            else if(punctuationInputCommandText.equals(words.get(0))) {
                processPunctuation(U.join(U.rest(words)));
                return;
            }
        }
        lastEditTextLength.push(textCapturedSoFar.length());
        if(isEmpty(textCapturedSoFar)) textCapturedSoFar = input;
        else textCapturedSoFar = hasTrailingSpace(textCapturedSoFar) ? textCapturedSoFar + input : textCapturedSoFar + " " + input;
        afterInputCapture();
    }

    private void processPunctuation(String input) {
        if(isEmpty(input)) return;
        String symbols = U.reduce(words(input), (String finalString, String currentWord) -> {
            return finalString + punctuation(currentWord, contextRef.get());
        }, "");
        lastEditTextLength.push(textCapturedSoFar.length());
        if(isEmpty(textCapturedSoFar)) textCapturedSoFar = symbols;
        else textCapturedSoFar = textCapturedSoFar +  symbols;
        afterInputCapture();
    }

    private void finalizeInput(){
        Choice gotMoreDigitsToFill = confirmConversation( "finalizing " + textCapturedSoFar, ttsWrapper, contextRef.get());
        ConversationProcessor.startConversation(gotMoreDigitsToFill, commandAndArguments -> {
            String commandChosen = commandAndArguments.param;
            if(commandAndArguments.command == CANCEL) {
                ConversationProcessor.endCurrentConversation(cancelConvParams());
            }
            else if(confirmCommandText.equalsIgnoreCase(commandChosen)) {
                Log.d("yolo text captured is", textCapturedSoFar);
                ConversationProcessor.endCurrentConversation(closeConvParams(textCapturedSoFar));
            }
            else if(noCommandText.equalsIgnoreCase(commandChosen)) {
                Tts.speakAfterQueue("so far we got", ttsWrapper);
                Tts.speakAfterQueue( textCapturedSoFar, ttsWrapper);
                Tts.speakAfterQueue( "proceed further", ttsWrapper);
            }
        });
    }
    private Pair<Command, String> closeConvParams(String text){
        return new Pair<>(inputType, text);
    }
}
