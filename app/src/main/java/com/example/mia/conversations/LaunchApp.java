package com.example.mia.conversations;

import static com.example.mia.Command.CANCEL;
import static com.example.mia.Command.INPUT_TEXT_TYPE_TEXT;
import static com.example.mia.Command.LAUNCH_APP;
import static com.example.mia.conversations.ConversationProcessor.cancelCurrentConversation;
import static com.example.mia.conversations.ConversationProcessor.endCurrentConversation;
import static com.example.mia.conversations.ConversationProcessor.startConversation;
import static com.example.mia.conversations.ConversationUtils.withTextCapture;
import static com.example.mia.utils.ListUtils.find;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import androidx.core.util.Pair;

import com.example.mia.utils.Tts;
import com.github.underscore.U;

import java.util.List;

public class LaunchApp extends Conversation {
    private final Context context;
    private final List<Pair<String, String>> listOfApps;

    public LaunchApp(Tts.TtsWrapper ttsWrapper, Context context) {
        super(ttsWrapper);
        this.context = context;
        listOfApps = listOfApps();
    }

    @Override
    public void processNextInput(String input) {
        converse();
    }

    private void converse() {
        Tts.speakAfterQueue("Give full name or part of the app you want to open", ttsWrapper);
        withTextCapture(partOfAppName -> {
            List<Pair<String, String>> filteredApps = U.filter(listOfApps, appInfoPair -> appInfoPair.first.toLowerCase().contains(partOfAppName.toLowerCase()));
            if(filteredApps.size() == 0){
                Tts.speakAfterQueue("No apps matched " + partOfAppName + " try again", ttsWrapper);
            } else if (filteredApps.size() == 1) {
                launchAppAndEndConversation(filteredApps.get(0));
            } else if(filteredApps.size() >5) {
                Tts.speakAfterQueue("More than 5 apps matched criteria, try again", ttsWrapper);
                converse();
            }
            else chooseApp(filteredApps);
        }, ConversationProcessor::cancelCurrentConversation, INPUT_TEXT_TYPE_TEXT, ttsWrapper, context);
    }

    private void chooseApp(List<Pair<String, String>> filteredApps) {
        Conversation choiceConversation = new ChoiceBasedOnIndex(getAppNamesFrom(filteredApps), ttsWrapper, context);
        startConversation(choiceConversation, commandAndArguments -> {
            if(commandAndArguments.command == CANCEL){
                cancelCurrentConversation();
            }
            String finalizedAppName = commandAndArguments.param;
            Pair<String, String> selectedAppInfo = find(filteredApps, appInfo -> appInfo.first.equalsIgnoreCase(finalizedAppName));
            launchAppAndEndConversation(selectedAppInfo);
        });
    }

    private void launchAppAndEndConversation(Pair<String, String> selectedAppInfo) {
        endCurrentConversation(new Pair<>(LAUNCH_APP, selectedAppInfo.second));
    }

    private List<String> getAppNamesFrom(List<Pair<String, String>> appInfos) {
        return U.map(appInfos, appInfo -> appInfo.first);
    }

    @Override
    public void start() {
        converse();
    }

    private List<Pair<String, String>> listOfApps(){
        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        return U.map(packages, appInfo -> new Pair<>(pm.getApplicationLabel(appInfo).toString(), appInfo.packageName));
    }
}
