package com.example.mia;

import static android.view.KeyEvent.FLAG_LONG_PRESS;
import static androidx.core.view.accessibility.AccessibilityEventCompat.TYPES_ALL_MASK;
import static com.example.mia.utils.Async.runAsync;
import static com.example.mia.utils.Java.safeCall;
import static com.example.mia.utils.Java.safeFunction;
import static com.example.mia.utils.MessageBus.MessageType.APP_ACTION_ERROR;
import static com.example.mia.utils.Tts.genericTts;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;

import com.example.mia.conversations.ConversationProcessor;
import com.example.mia.utils.MessageBus;
import com.example.mia.utils.Tts;
import com.example.mia.utils.WakeLockUtils;
import com.github.underscore.Consumer;

import java.util.Locale;

public class NaansAccessibilityService extends AccessibilityService {
    private TextToSpeech tts;
    private Tts.TtsWrapper genericTtsWrapper;
    private NaansInputService naansInputService;
    private boolean naansServiceInitialized = false;
    public static boolean serviceConnected = false;

    private final Consumer<Object> onAppActionError = errorMessage -> {
        Tts.speakAfterQueue(errorMessage.toString(), genericTtsWrapper);
    };

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
//        Log.d("Naans acc service", event.toString());
    }

    @Override
    public void onInterrupt() {
        Log.d("yolo naans service", "service interrupted");
        safeCall(this::shutdownOurServiceAndResources, e -> {
            Tts.speakAfterQueue("Failed to shutdown service", genericTtsWrapper);
        });
    }

    @Override
    public void onDestroy() {
        Log.d("yolo", "service destroyed");
        safeCall(this::shutdownOurServiceAndResources, e -> {
            Tts.speakAfterQueue("Failed to shutdown service", genericTtsWrapper);
        });
        super.onDestroy();
    }

    private void shutdownOurServiceAndResources() {
        if(tts !=null) tts.shutdown();
        WakeLockUtils.removeTempScreenWakeLock();
        if(naansInputService != null) naansInputService.stop();
        ConversationProcessor.cleanup();
        naansInputService = null;
        MessageBus.unsubscribe(APP_ACTION_ERROR, onAppActionError);
        serviceConnected = false;
    }

    @Override
    public void onServiceConnected() {
        safeCall(this::onServiceConnect, e -> {
            Log.e("naans service", "failed to start");
            e.printStackTrace();
            Tts.speakAfterQueue("Failed to start service", genericTtsWrapper);
        });
    }

    private void onServiceConnect() {
        serviceConnected = true;
        MessageBus.subscribe(APP_ACTION_ERROR, onAppActionError);
        // TODO: handle "should enter sleep mode" event
        tts = new TextToSpeech(this, status -> {
            if(status == TextToSpeech.ERROR) {
                Log.e("naans tts", "tts error on initialize");
                return;
            }
            genericTtsWrapper = Tts.genericTts(tts);
            tts.setLanguage(new Locale("en", "IN"));
            Tts.speakAfterQueue("Audio initialized", genericTtsWrapper);
            if(naansInputService == null)
                runAsync(() -> naansInputService = new NaansInputService(this, () -> naansServiceInitialized = true, tts));
        });
        genericTtsWrapper = genericTts(tts);
        if(isInputDeviceBluetooth()) WakeLockUtils.acquireDimScreenWakeLock(this);
        Log.d("naans service yolo", "accessibility service connected");
        // Set the type of events that this service wants to listen to. Others
        // aren't passed to this service.
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        if(BuildConfig.DEBUG) info.eventTypes = TYPES_ALL_MASK;

        // Set the type of feedback your service provides.
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_SPOKEN;
        info.flags |= AccessibilityServiceInfo.DEFAULT // TODO: seems fishy, is this required?
//                | AccessibilityServiceInfo.CAPABILITY_CAN_REQUEST_TOUCH_EXPLORATION // TODO: probably perform gestures later
                | AccessibilityServiceInfo.FLAG_REQUEST_FILTER_KEY_EVENTS;
//                | AccessibilityServiceInfo.FLAG_RETRIEVE_INTERACTIVE_WINDOWS;

        if(Build.VERSION.SDK_INT >= 24) info.flags |= AccessibilityServiceInfo.CAPABILITY_CAN_PERFORM_GESTURES;
        info.notificationTimeout = 100;

        this.setServiceInfo(info);
    }

    private boolean isInputDeviceBluetooth() {
        return true; // TODO: impl later
    }


    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        return safeFunction(this::handleKeyEvent, event, e -> {
            Tts.speakAfterQueue("Failed to handle keyevent", genericTtsWrapper);
            return false;
        });
    }

    private boolean handleKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
//        if(!isDPADButton(keyCode)) return false; //TODO: probably look for only few buttons
        boolean isLongPress = (event.getFlags() & FLAG_LONG_PRESS) != 0;
        if(isLongPress) Log.d("yolo", "long pressed ");
        // TODO: handle long press
        return naansInputService.onPressed(event);
    }
}
