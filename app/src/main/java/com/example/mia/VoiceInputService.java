package com.example.mia;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.text.TextUtils.isEmpty;
import static com.example.mia.Command.DEBUG_LAST_COMMAND;
import static com.example.mia.Command.INPUT_TEXT_TYPE_TEXT;
import static com.example.mia.Command.PROCESS_VOICE_INPUT;
import static com.example.mia.Command.commandNameToCommandMapping;
import static com.example.mia.Command.createTextToCommandMapping;
import static com.example.mia.VoiceAutomationInputService.getNextAction;
import static com.example.mia.VoiceAutomationInputService.hasAutoCommands;
import static com.example.mia.command.CommandUtils.matches;
import static com.example.mia.conversations.ChoiceBasedOnIndex.uponChoosing;
import static com.example.mia.conversations.ConversationProcessor.conversationsGoingOn;
import static com.example.mia.conversations.ConversationUtils.uponSuccessfulTextCapture;
import static com.example.mia.conversations.ConversationUtils.withTextCapture;
import static com.example.mia.utils.Android.announceError;
import static com.example.mia.utils.AppActionUtils.conveyChoice;
import static com.example.mia.utils.AppActionUtils.trigger;
import static com.example.mia.utils.Async.runAsync;
import static com.example.mia.utils.Async.runOnMainDelayed;
import static com.example.mia.utils.Java.safeCall;
import static com.example.mia.utils.MessageBus.MessageType.APP_ACTION_CHOOSE_REQUEST;
import static com.example.mia.utils.MessageBus.MessageType.ENTERING_SLEEP_MODE;
import static com.example.mia.utils.MessageBus.MessageType.EXITING_SLEEP_MODE;
import static com.example.mia.utils.Tts.genericTts;
import static com.example.mia.utils.Tts.getTtsThatWorksDuringWorkflow;
import static com.example.mia.workflows.Action.hasDynamicInput;

import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import com.example.mia.Exceptions.WorkflowAlreadyExists;
import com.example.mia.appactions.AppAction;
import com.example.mia.command.CommandUtils;
import com.example.mia.conversations.ConversationProcessor;
import com.example.mia.utils.Java.FunctionThatThrows;
import com.example.mia.utils.MessageBus;
import com.example.mia.utils.Tts;
import com.example.mia.voice.DefaultVoiceRecognitionService;
import com.example.mia.voice.InAppVoiceRecognitionService;
import com.example.mia.voice.VoiceRecognitionService;
import com.example.mia.workflows.Action;
import com.example.mia.workflows.AppActionsStore;
import com.example.mia.workflows.Workflow;
import com.example.mia.workflows.WorkflowStore;
import com.github.underscore.Consumer;
import com.github.underscore.U;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class VoiceInputService {
    private final LinkedList<String> recordings;
    private final int maxRecordingsSize = 100;
    private TextToSpeech tts;
    WeakReference<MyAccessibilityService> accessibilityServiceRef;
    private Map<String, Command> textToCommandMapping;
    private Consumer<CommandAndArguments> onCommand;
    public static Action currentAutomationAction;
    public static boolean processingWorkflow = false;
    private VoiceRecognitionService defaultVoiceRecognitionService;
    private VoiceRecognitionService inAppVoiceRecognitionService;
    private VoiceRecognitionService currentVoiceRecognitionService;

    private final Consumer<Object> onAppActionChoose = choicesObj -> {
        List<String> choices = (List<String>) choicesObj;
        uponChoosing(choices, choice -> choice, choice -> {
            conveyChoice(choices.indexOf(choice), ongoingAppAction, accessibilityServiceRef.get());
        }, () -> {
            conveyChoice(-1, ongoingAppAction, accessibilityServiceRef.get());
        }, genericTts(tts), accessibilityServiceRef.get());
    };

    private final Consumer<Object> onExitingSleepMode = ignore -> {
        Log.d("voice input", "exiting sleep mode - switching voice recog services");
        currentVoiceRecognitionService.pause();
        currentVoiceRecognitionService = defaultVoiceRecognitionService;
        currentVoiceRecognitionService.resume();
    };

    private final Consumer<Object> onEnteringSleepMode = ignore -> {
        Log.d("voice input", "entering sleep mode - switching voice recog services");
        currentVoiceRecognitionService.pause();
        currentVoiceRecognitionService = inAppVoiceRecognitionService;
        currentVoiceRecognitionService.resume();
    };

    private Runnable processAndScheduleNextCommand = () -> {
        currentAutomationAction = getNextAction();
        Tts.TtsWrapper ttsForAction = getTtsForAction(currentAutomationAction);
        if(!hasDynamicInput(currentAutomationAction)){
            processAction(currentAutomationAction, ttsForAction);
            scheduleNextCommand(currentAutomationAction.delayForNextActionInMillis);
            return;
        }
        Log.d("yolo automation", "capturing dynamic input start");
        Tts.speakCancellingQueue(currentAutomationAction.dynamicInput.question, ttsForAction);
        resumeHearingIfNotProcessingWorkflow(); // odd name to call from workflow;
        withTextCapture(txt -> {
           processAction(new Action(currentAutomationAction.command, txt), ttsForAction);
            scheduleNextCommand(currentAutomationAction.delayForNextActionInMillis);
        }, this::onWorkflowInputCaptureCancelled, currentAutomationAction.dynamicInput.inputType, ttsForAction, accessibilityServiceRef.get());
    };
    private CustomUtteranceListener utteranceListener;
    private static AppAction ongoingAppAction; // TODO: this stays forever even after action is complete
    private boolean recording = false;
    private String lastCommand = "";

    private Tts.TtsWrapper getTtsForAction(Action action) {
        return hasDynamicInput(action) || !action.shouldDisableTts ? getTtsThatWorksDuringWorkflow(tts) : genericTts(tts);
    }

    private void onWorkflowInputCaptureCancelled(){
        processingWorkflow = false;
        currentAutomationAction = null;
        VoiceAutomationInputService.cancel();
        resumeHearingIfNotProcessingWorkflow();
    }

    private void processAction(Action nextCommand, Tts.TtsWrapper ttsWrapper) {
        Log.d("yolo automation", "processing next auto command: " + nextCommand.command.toString() + ", param: " + nextCommand.param);
        if(nextCommand.command == PROCESS_VOICE_INPUT) onProcessVoiceInput(nextCommand.param, ttsWrapper);
        else onCommand.accept(new CommandAndArguments(nextCommand.command, nextCommand.param, ttsWrapper));
    }

    private void scheduleNextCommand(int delayInMillis) {
        if (hasAutoCommands()) {
            runOnMainDelayed(this.processAndScheduleNextCommand, delayInMillis);
        }
        else {
            processingWorkflow = false;
            currentAutomationAction = null;
            runOnMainDelayed(this::resumeHearingIfNotProcessingWorkflow, 500);
        }
    }


    public VoiceInputService(MyAccessibilityService service, Runnable onReadyToListen, TextToSpeech tts) {
        this.tts = tts;
        this.recordings = new LinkedList<>();
        accessibilityServiceRef = new WeakReference<>(service);
        runAsync(() -> {
            textToCommandMapping = createTextToCommandMapping(service);
            InAppVoiceRecognitionService.initModel(service, onReadyToListen, ex -> {
                announceError("Failed loading InApp voice recognition", tts);
            });
        });
    }


    private void pauseRecordingWhileSpeakingOutNodes(TextToSpeech tts) {
        utteranceListener = new CustomUtteranceListener();
        tts.setOnUtteranceProgressListener(utteranceListener);
    }

    public void startRecording() {
        this.recording = true;
    }

    public void stopRecording() {
        this.recording = false;
        createWorkflowFromRecordings();
    }

    private void createWorkflowFromRecordings() {
        Tts.speakAfterQueue("Pick a name for this workflow", Tts.genericTts(tts));
        uponSuccessfulTextCapture(workflowName -> {
            try {
                WorkflowStore.addWorkflow(Workflow.createWorkflow(workflowName, new ArrayList<>(recordings)), accessibilityServiceRef.get());
                Tts.speakAfterQueue("Added workflow with name " + workflowName, genericTts(tts));
            } catch (WorkflowAlreadyExists e) {
                Tts.speakAfterQueue("Workflow with this name already exists, pick a new name", genericTts(tts));
                createWorkflowFromRecordings();
            }
        }, INPUT_TEXT_TYPE_TEXT, genericTts(tts), accessibilityServiceRef.get());
    }

    public void stopRecognizing(){
        processingWorkflow = false;
        currentAutomationAction = null;
        VoiceAutomationInputService.cleanUp();
        utteranceListener.stop();
        tts = null;
        MessageBus.unsubscribe(ENTERING_SLEEP_MODE, onEnteringSleepMode);
        MessageBus.unsubscribe(EXITING_SLEEP_MODE, onExitingSleepMode);
        MessageBus.unsubscribe(APP_ACTION_CHOOSE_REQUEST, onAppActionChoose);
        currentVoiceRecognitionService = null;
        inAppVoiceRecognitionService.shutdown();
        defaultVoiceRecognitionService.shutdown();
        inAppVoiceRecognitionService = null;
        defaultVoiceRecognitionService = null;
    }


    public void startRecognizing(MyAccessibilityService service, Consumer<CommandAndArguments> onCommand) {
        this.onCommand = onCommand;
        accessibilityServiceRef = new WeakReference<>(service);
        Log.d("yolo voice", "start recognizing");
        int permissionCheck = ContextCompat.checkSelfPermission(accessibilityServiceRef.get(), RECORD_AUDIO);
        if(permissionCheck != PermissionChecker.PERMISSION_GRANTED) {
            announceError("Please grant audio permission to proceed", tts);
            return;
        }
        MessageBus.subscribe(ENTERING_SLEEP_MODE, onEnteringSleepMode);
        MessageBus.subscribe(EXITING_SLEEP_MODE, onExitingSleepMode);
        MessageBus.subscribe(APP_ACTION_CHOOSE_REQUEST, onAppActionChoose);
        if(currentVoiceRecognitionService == null){
            safeCall(() -> {
                pauseRecordingWhileSpeakingOutNodes(tts);
                defaultVoiceRecognitionService = new DefaultVoiceRecognitionService(service, this::resumeHearingIfNotProcessingWorkflow, ex -> announceError("Failed starting default voice recognition", tts),this::onResultFromVoiceRecognition);
                currentVoiceRecognitionService = defaultVoiceRecognitionService;
                inAppVoiceRecognitionService = new InAppVoiceRecognitionService(service, () -> {}, ex -> announceError("Failed loading inapp voice recognition", tts), this::onResultFromVoiceRecognition);
                processAutoCommandsWhenAvailable();
                Tts.speakAfterQueue("Voice input ready", genericTts(tts));
            }, e -> {
                announceError("failed to start speech service", tts);
                Log.e("yolo", "failed to start speech service" + e.getMessage());
            });
        }
        else{
            resumeHearingIfNotProcessingWorkflow();
        }
    }

    private void onResultFromVoiceRecognition(String hypothesis) {
        Log.d("yolo voice voice", "result: " + hypothesis);
        String text = hypothesis.toLowerCase();
        processVoiceInput(text
                , genericTts(tts));
    }

    private void processAutoCommandsWhenAvailable() {
        VoiceAutomationInputService.setOnVoiceCommandsAvailable(() -> {
            if(hasAutoCommands()) processWorkflow();
        });
    }

    private void processWorkflow() {
        processingWorkflow = true;
        currentAutomationAction = null;
        if(currentVoiceRecognitionService != null) currentVoiceRecognitionService.pause();
        Log.d("yolo automation", "processing automation");
        runOnMainDelayed(processAndScheduleNextCommand, 0);
    }

    private void processVoiceInput(String text, Tts.TtsWrapper ttsWrapper) {
        safeCall(() -> {
            onProcessVoiceInput(text, ttsWrapper);
        }, e -> {
            Tts.speakAfterQueue("Couldn't process voice input " + text, ttsWrapper);
        });
    }

    private void onProcessVoiceInput(String text, Tts.TtsWrapper ttsWrapper) {
        if(currentVoiceRecognitionService != null) currentVoiceRecognitionService.pause();
        if (CommandUtils.matches(text, DEBUG_LAST_COMMAND)) {
            String heard = isEmpty(lastCommand) ? "Nothing" : lastCommand;
            Tts.speakAfterQueue("heard " + heard, ttsWrapper);
            resumeHearingIfNotProcessingWorkflow();
            return;
        }
        lastCommand = text;
        if(recording && !processingWorkflow) {
            if(!isEmpty(text)) recordings.addLast(text);
            if(recordings.size() > maxRecordingsSize) recordings.removeFirst();
        }
        if(conversationsGoingOn()){
           ConversationProcessor.processNextInput(text);
            resumeHearingIfNotProcessingWorkflow();
            return;
        }
        if (isEmpty(text)) {
            Tts.speakAfterQueue("Couldn't understand or hear", ttsWrapper);
            resumeHearingIfNotProcessingWorkflow();
            return;
        }
        Workflow workflow = WorkflowStore.getWorkflow(text, accessibilityServiceRef.get());
        if(workflow != null){
            VoiceAutomationInputService.run(workflow);
            return;
        }
        AppAction appAction = AppActionsStore.getAppAction(text, accessibilityServiceRef.get());
        if(appAction !=null) {
            ongoingAppAction = appAction;
            trigger(text, appAction, accessibilityServiceRef.get());
            resumeHearingIfNotProcessingWorkflow();
            return;
        }
//        ongoingAppAction = null; //TODO: we need to nullify on action end
        Command voiceCommand = getMatchingVoiceCommand(text);
        if(voiceCommand == null) {
            Tts.speakAfterQueue(text + " didn't match any command supported", ttsWrapper);
            resumeHearingIfNotProcessingWorkflow();
            return;
        }
        if (voiceCommand.commandDefinition.isConversation) {
            ConversationProcessor.processConversation(voiceCommand, text,ttsWrapper, accessibilityServiceRef.get(), onCommand);
            resumeHearingIfNotProcessingWorkflow();
            return;
        }
        onCommand.accept(new CommandAndArguments(voiceCommand, "", text, ttsWrapper));
        resumeHearingIfNotProcessingWorkflow();
    }

    private void resumeHearingIfNotProcessingWorkflow() {
        if(!processingWorkflow || hasDynamicInput(currentAutomationAction)) utteranceListener.scheduleNextRun();
    }

    private @Nullable Command getMatchingVoiceCommand(String text) {
        return U.find(commandNameToCommandMapping.values(), command -> matches(text, command))
                .orNull();
    }

    class CustomUtteranceListener extends UtteranceProgressListener {
        private final int START_HEARING_AFTER_TIMEOUT_MS = accessibilityServiceRef.get().getResources().getInteger(R.integer.start_hearing_after_tts_timeout_ms);
        private boolean shouldScheduleNext = true;
        private long recentUtteranceTime = 0;
        private final FunctionThatThrows resumeHearingWhenItsRightTime = () -> {
            if(tts == null || currentVoiceRecognitionService == null) return;
            if (!currentVoiceRecognitionService.paused)  return;
            if(tts.isSpeaking() || hadSomeUtteranceJustNow()){
                scheduleNextRun();
                return;
            }
            currentVoiceRecognitionService.resume();
        };

        private boolean hadSomeUtteranceJustNow() {
            return System.currentTimeMillis() - recentUtteranceTime <= START_HEARING_AFTER_TIMEOUT_MS;
        }

        CustomUtteranceListener(){}

        private void scheduleNextRun() {
            if(!shouldScheduleNext) return;
            runOnMainDelayed(() -> safeCall(resumeHearingWhenItsRightTime, e -> {
                e.printStackTrace();
                announceError("Failed to resume hearing", tts);
            }), START_HEARING_AFTER_TIMEOUT_MS);
        }

        public void stop() {
            this.shouldScheduleNext = false;
        }

        @Override
        public void onStart(String utteranceId) {
            recentUtteranceTime = System.currentTimeMillis();
            if (currentVoiceRecognitionService == null) return;
            if(currentVoiceRecognitionService.paused) return;
            Log.d("yolo voice", "pausing");
            stopHearing();
            scheduleNextRun();
        }

        private void stopHearing() {
            safeCall(() -> {
                if (currentVoiceRecognitionService == null) return;
                currentVoiceRecognitionService.pause();
            }, e -> {
                e.printStackTrace();
                announceError("Failed to stop voice recognition", tts);
            });
        }

        @Override
        public void onDone(String utteranceId) {
            recentUtteranceTime = System.currentTimeMillis();
        }

        @Override
        public void onError(String utteranceId) {
            recentUtteranceTime = System.currentTimeMillis();
        }

    }

    @Nullable
    public static AppAction getOnGoingAppAction() {
        return ongoingAppAction;
    }


}
