package com.example.mia.voice;

import android.content.Context;

import com.github.underscore.Consumer;


public abstract class VoiceRecognitionService {
    public Context context;
    public final Consumer<Exception> onInitFailure;
    public Consumer<String> onAudioReceive;
    public Runnable onReady;
    public boolean paused;

    public VoiceRecognitionService(Context context, Runnable onReady, Consumer<Exception> onInitFailure, Consumer<String> onAudioReceive) {
        this.context = context;
        this.onInitFailure = onInitFailure;
        this.onReady = onReady;
        this.onAudioReceive = onAudioReceive;
    }

    public void pause(){
        onPause();
        paused = true;
    };


    protected abstract void onPause();

    public void resume(){
        onResume();
        paused = false;
    }

    protected abstract void onResume();

    public abstract void shutdown();

}
