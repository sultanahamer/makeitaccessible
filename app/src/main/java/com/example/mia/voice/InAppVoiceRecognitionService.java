package com.example.mia.voice;

import static com.example.mia.utils.Android.showToastError;

import android.content.Context;
import android.util.Log;


import com.github.underscore.Consumer;

import org.vosk.LibVosk;
import org.vosk.LogLevel;
import org.vosk.Model;
import org.vosk.Recognizer;
import org.vosk.android.RecognitionListener;
import org.vosk.android.SpeechService;
import org.vosk.android.StorageService;

import java.io.IOException;

public class InAppVoiceRecognitionService extends VoiceRecognitionService {

    private SpeechService speechService;
    private static Model voiceRecognitionModel;

    public InAppVoiceRecognitionService(Context context, Runnable onReady, Consumer<Exception> onInitFailure, Consumer<String> onAudioReceive) {
        super(context, onReady, onInitFailure, onAudioReceive);
        initService(context);
        onReady.run();
    }

    private void initService(Context context) {
        Recognizer rec;
        try {
            rec = new Recognizer(voiceRecognitionModel, 16000.0f);
            speechService = new SpeechService(rec, 16000.0f);
        } catch (IOException e) {
            e.printStackTrace();
            showToastError(e.getMessage(), context);
            onInitFailure.accept(e);
            Log.e("yolo", "failed to start speech service" + e.getMessage());
        }

        speechService.startListening(new RecognitionListener() {
            @Override
            public void onPartialResult(String hypothesis) {
//                Log.d("yolo voice voice", "partial result: " + hypothesis);
            }

            @Override
            public void onResult(String hypothesis) {
                String text = hypothesis.substring(14, hypothesis.length() - 3); // the voice recog service gives content as json string hence the parsing
                Log.d("inapp voice", "received " + text);
                onAudioReceive.accept(text);
            }

            @Override
            public void onFinalResult(String hypothesis) {
//                Log.d("yolo voice voice", "final result: " + hypothesis);
            }

            @Override
            public void onError(Exception exception) {
                exception.printStackTrace();
                Log.e("inapp voice", "error" + exception.getMessage());
                showToastError(exception.getMessage(), context);
            }

            @Override
            public void onTimeout() {
                Log.d("inapp voice", "timeout");
            }
        });
        speechService.setPause(true);
    }

    public static void initModel(Context context, Runnable onReady, Consumer<Exception> onInitFailure) {
        Log.d("yolo voice", "initing model...");
        LibVosk.setLogLevel(LogLevel.INFO);
        StorageService.unpack(context, "model-en-us", "model",
                (model) -> {
                    voiceRecognitionModel = model;
                    Log.d("inapp voice", "voice model loaded");
                    onReady.run();
                },
                (exception) -> {
                    exception.printStackTrace();
                    Log.e("inapp voice", "failed loading model" + exception.getMessage());
                    onInitFailure.accept(exception);
                });
    }


    @Override
    protected void onPause() {
        Log.d("inapp voice", "pausing");
        if (speechService == null) return;
        speechService.setPause(true);
    }

    @Override
    public void shutdown() {
        Log.d("inapp voice", "shutdown");
        speechService.stop();
        speechService.shutdown();
    }

    @Override
    protected void onResume() {
        Log.d("inapp voice", "resuming");
        if (speechService == null) return;
        speechService.setPause(false);
    }
}
