package com.example.mia.voice;

import static com.example.mia.utils.Async.runOnMain;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.github.underscore.Consumer;
import com.github.underscore.U;

import java.util.ArrayList;

public class DefaultVoiceRecognitionService extends VoiceRecognitionService{
    private boolean listening = false;
    private boolean invalidateResults = false;
    private SpeechRecognizer speechRecognizer;
    private Intent speechRecognizerIntent;
    public DefaultVoiceRecognitionService(Context context, Runnable onReady, Consumer<Exception> onFailure, Consumer<String> onAudioReceive) {
        super(context, onReady, onFailure, onAudioReceive);
        runOnMain(this::init);
    }

    @Override
    protected void onPause() {
        if (!listening) return;
        invalidateResults = true;
        runOnMain(speechRecognizer::cancel);
    }

    @Override
    public void shutdown() {
        if(speechRecognizer == null) return;
        speechRecognizer.destroy();
        speechRecognizer = null;
    }

    @Override
    protected void onResume() {
        if(listening) return;
        runOnMain(() -> {
            listening = true;
            Log.d("goog", "started listening");
            if(speechRecognizer == null) return;
            speechRecognizer.startListening(speechRecognizerIntent);
        });

    }
    private void init() {
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US"); // TODO: ask user and use it
        speechRecognizer.setRecognitionListener(new android.speech.RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {
                System.out.println("google error:" + error);
//                showToastError( "error code received: " + error, context);
                listening = false;
                if(invalidateResults) {
                    invalidateResults = false;
                    return;
                }
                onAudioReceive.accept("");
            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> texts = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                U.forEach(texts, text -> System.out.println("yolo google voice:" + text));
                listening = false;
                if(invalidateResults) {
                    invalidateResults = false;
                    return;
                }
                onAudioReceive.accept(texts.get(0));
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });
        onReady.run();
    }
}
