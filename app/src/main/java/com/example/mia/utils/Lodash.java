package com.example.mia.utils;

import com.github.underscore.Function;

import java.util.ArrayList;
import java.util.List;

public class Lodash {
    public static <T, R> List<R> mapNonNull(Function<T, R> mapper, Iterable<T> items){
        ArrayList<R> finalResult = new ArrayList<>();
        for(T item : items){
            R result = mapper.apply(item);
            if(result == null) continue;
            finalResult.add(result);
        }
        return finalResult;
    }
}
