package com.example.mia.utils;

import android.os.Handler;
import android.os.Looper;

public class Async {
    public static Handler mainHandler = new Handler(Looper.getMainLooper());
//    private static Handler greenThreadHandler;
//    private static Thread greenThread = new Thread(){
//        @Override
//        public void run() {
//            Looper.prepare();
//            greenThreadHandler = new Handler(Looper.myLooper());
//            Looper.loop();
//        }
//    };
//    static {
//        greenThread.start();
//    }

    public static void runOnMain(Runnable r){
        mainHandler.post(r);
    }
    public static void runOnMainDelayed(Runnable r, long delayInMillis){
        mainHandler.postDelayed(r, delayInMillis);
    }
    public static void runAsync(Runnable r) {
        if(r == null) return;
        new Thread(){
            @Override
            public void run() {
                r.run();
            }
        }.start();
    }


//    public static void runSmallTaskOnGreenThread(Runnable runnable){
//        greenThreadHandler.post(runnable);
//    }

//    public static void runSmallTaskOnGreenThreadDelayed(Runnable runnable, int delayInMillis){
//        greenThreadHandler.postDelayed(runnable, delayInMillis);
//    }

//    public interface Condition {
//       boolean isMet();
//    }
//    public static void loopOnGreenThread(Runnable runnable, Condition condition, int delayInMillis){
//        new LooperHelper(runnable, condition, delayInMillis).loop();
//    }
//
//    public static class LooperHelper {
//        private Runnable helper;
//
//        LooperHelper(Runnable toRun, Condition condition, int delayInMillis){
//            helper = () -> {
//                if(condition.isMet()) return;
//                toRun.run();
//                runSmallTaskOnGreenThreadDelayed(this.helper, delayInMillis);
//            };
//        }
//        public void loop(){
//            runSmallTaskOnGreenThread(helper);
//        }
//
//    }
}
