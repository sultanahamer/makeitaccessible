package com.example.mia.utils;

import static com.example.mia.utils.Java.safeCall;

import android.util.Log;

import com.github.underscore.Consumer;
import com.github.underscore.U;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageBus {
    public enum MessageType {
       ENTERING_SLEEP_MODE, EXITING_SLEEP_MODE, WORKFLOWS_UPDATED, APP_ACTION_ERROR, APP_ACTION_CHOOSE_REQUEST, SHOULD_ENTER_SLEEP_MODE
    }
    private static Map<MessageType, List<Consumer<Object>>> listenersMappedByMessage = new HashMap<>(3);

    public static void broadcast(MessageType messageType) {
        broadcast(messageType, null);
    }
    public static void broadcast(MessageType messageType, Object message) {
        U.forEach(listenersFor(messageType), listener-> safeCall(() -> listener.accept(message), e -> {
            e.printStackTrace();
            Log.e("message bus", "failed to broadcast", e);
        }));
    }

    private static List<Consumer<Object>> listenersFor(MessageType messageType) {
        if(!listenersMappedByMessage.containsKey(messageType)) {
            listenersMappedByMessage.put(messageType, new ArrayList<>(3));
        }
        return listenersMappedByMessage.get(messageType);
    }
    public static void subscribe(MessageType messageType, Consumer<Object> listener) {
        // TODO: Consume<Object> is kinda bad, look for specifying type MessageType<T> should be able to do it
        List<Consumer<Object>> listeners = listenersFor(messageType);
        listeners.remove(listener);
        listeners.add(listener);
    }

    public static void unsubscribe(MessageType messageType, Consumer<Object> listener) {
        List<Consumer<Object>> listeners = listenersFor(messageType);
        listeners.remove(listener);
    }
}
