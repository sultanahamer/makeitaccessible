package com.example.mia.utils;

import static com.example.mia.utils.Android.announceError;
import static com.example.mia.utils.Android.getApplicationName;
import static com.example.mia.utils.Tts.genericTts;
import static java.util.Arrays.asList;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.service.notification.StatusBarNotification;
import android.speech.tts.TextToSpeech;

import com.example.mia.NotificationsListenerService;
import com.github.underscore.Consumer;
import com.github.underscore.U;

import java.util.ArrayList;
import java.util.List;

public class StatusBarNotificationUtils {

    public static final String DISMISS_NOTIFICATION = "DISMISS";
    public static final String CLICK_NOTIFICATION = "CLICK";

    public static String getTitle(StatusBarNotification notification) {
        return notification.getNotification().extras.getString(Notification.EXTRA_TITLE, "");
    }

    public static String getText(StatusBarNotification notification) {
//        String content = "";
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Parcelable[] messageBundles = notification.getNotification().extras.getParcelableArray(Notification.EXTRA_MESSAGES);
//            if (messageBundles != null) {
//                List<String> messages = U.map(asList(messageBundles), parcel -> {
//                    Bundle bundle = (Bundle) parcel;
//                    return bundle.getString("sender") + " said " + bundle.getString("text");
//                });
//                content += U.join(messages, ",");
//            }
//            else {
//                content = notification.getNotification().extras.getString(Notification.EXTRA_TEXT, "");
//            }
//            }
//            else {
//                notification.getNotification().extras.getParcelable(Notification.EXTRA_MESSAGES);
//            }
//        return content;
        return notification.getNotification().extras.getString(Notification.EXTRA_TEXT, "");
    }

    public static String getAppName(StatusBarNotification notification, Context context) {
        return getApplicationName(notification.getPackageName(), context);
    }

    public static List<CharSequence> getActions(StatusBarNotification notification) {
        return U.chain(notification.getNotification().actions).map(x -> x.title).value();
    }

    public static void performAction(String action, Consumer<Exception> onFailure, StatusBarNotification notification) {
        Notification.Action actionObject = U.firstOrNull(asList(notification.getNotification().actions), x -> x.title == action);
        if(actionObject == null) {
            onFailure.accept(new Exception("No such action found"));
            return;
        }
        performAction(actionObject, onFailure);
    }
    public static void performAction(Notification.Action action, Consumer<Exception> onFailure) {
        try {
            action.actionIntent.send();
        } catch (PendingIntent.CanceledException e) {
            onFailure.accept(new Exception("Error performing action"));
        }
    }

    public static void announceNotification(StatusBarNotification notification, Tts.TtsWrapper ttsWrapper, Context context){
        Tts.speakAfterQueue("Notification from", ttsWrapper);
        Tts.speakAfterQueue(getAppName(notification, context), ttsWrapper);
        Tts.speakAfterQueue("title", ttsWrapper);
        Tts.speakAfterQueue(getTitle(notification),  ttsWrapper);
        Tts.speakAfterQueue("content", ttsWrapper);
        Tts.speakAfterQueue(getText(notification),  ttsWrapper);
    }


    public static List<NotificationAction> notificationActions(StatusBarNotification notification, TextToSpeech tts) {
        List<NotificationAction> actions = new ArrayList<>(3);
        if(notification.getNotification().actions != null) {
            List<NotificationAction> specificActions = U.map(asList(notification.getNotification().actions), action -> notificationAction(action, tts));
            actions.addAll(specificActions);
        }
        actions.addAll(genericNotificationActions(notification, tts));
        return actions;
    }

    private static NotificationAction notificationAction(Notification.Action action, TextToSpeech tts) {
        return new NotificationAction(action.title.toString(), ignore -> {
            try {
                action.actionIntent.send();
            } catch (PendingIntent.CanceledException e) {
                announceError("Error performing action " + action.title, tts);
            }
        });

    }

    private static List<NotificationAction> genericNotificationActions(StatusBarNotification notification, TextToSpeech tts) {
        Tts.TtsWrapper ttsWrapper = genericTts(tts);
        List<NotificationAction> actions = new ArrayList<>(2);
        if(notification.isClearable()) {
            actions.add(new NotificationAction(DISMISS_NOTIFICATION, notification1 -> {
                NotificationsListenerService.dismissNotification(notification1);
                Tts.speakAfterQueue("Performed action " + DISMISS_NOTIFICATION, ttsWrapper);
            }));
        }
        if(notification.getNotification().contentIntent != null) {
            actions.add(new NotificationAction(CLICK_NOTIFICATION, ignore -> {
                try {
                    notification.getNotification().contentIntent.send();
                    Tts.speakAfterQueue("Performed action " + CLICK_NOTIFICATION, ttsWrapper);
                } catch (PendingIntent.CanceledException e) {
                    announceError("Error performing action " + CLICK_NOTIFICATION, tts);
                }
            }));
        }
        actions.add(new NotificationAction("IGNORE", ign -> {
            Tts.speakAfterQueue("Ignored notification", ttsWrapper);
        }));
        return actions;
    }

    public static class NotificationAction {
        public String actionTitle;
        public Consumer<StatusBarNotification> onAction;

        public NotificationAction(String actionTitle, Consumer<StatusBarNotification> onAction) {
            this.actionTitle = actionTitle;
            this.onAction = onAction;
        }
    }



}
