package com.example.mia.utils;

import static android.text.InputType.TYPE_CLASS_NUMBER;
import static android.text.InputType.TYPE_CLASS_PHONE;
import static android.view.accessibility.AccessibilityNodeInfo.ACTION_SET_TEXT;

import static androidx.core.view.accessibility.AccessibilityNodeInfoCompat.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE;
import static com.example.mia.Command.INPUT_TEXT_TYPE_NUMBER;
import static com.example.mia.Command.INPUT_TEXT_TYPE_TEXT;
import static com.example.mia.utils.Text.isEmpty;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.util.Predicate;

import com.example.mia.Command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class AccessibilityNodeUtils {

    public static int getIndexInParent(AccessibilityNodeInfo node) {
        AccessibilityNodeInfo parent = node.getParent();
        if (!isValidNode(node) || parent == null) return -1;
        int childCount = parent.getChildCount();
        int positionInParent = -1;
        for (int i = 0; i < childCount; i++) {
            if (!parent.getChild(i).equals(node)) continue;
            positionInParent = i;
        }
        return positionInParent;
    }

    public static AccessibilityNodeInfo siblingNext(AccessibilityNodeInfo node) {
        AccessibilityNodeInfo parent = node.getParent();
        if (parent == null) return null;
        int childCount = parent.getChildCount();
        if (childCount == 1) { // no other sibling present
            return siblingNext(parent);
        }
        int positionInParent = getIndexInParent(node);
        if (positionInParent == -1) {
            Log.e("yolo", "couldn't find self in parent, bigger problem");
            return parent;
        }
        if (positionInParent + 1 == childCount) { // last child so no more siblings
            return siblingNext(parent);
        }
        for (int i = positionInParent + 1; i < childCount; i++) {
            AccessibilityNodeInfo focusableSibling = firstFocusableNode(parent.getChild(i));
            if (focusableSibling == null) continue;
            return focusableSibling;
        }
        return siblingNext(parent); // no sibling had focusable nodes
    }

    @Nullable
    public static AccessibilityNodeInfo firstFocusableNode(AccessibilityNodeInfo node) {
        if (!isValidNode(node)) return null;

        if (isHighlightable(node)) return node;
        return firstFocusableChild(node);
    }

    public static boolean isValidNode(AccessibilityNodeInfo node) {
        if (node == null) return false;
        return nodeStillAvaialble(node);
    }

    public static boolean nodeStillAvaialble(AccessibilityNodeInfo nodeInfo) {
        return nodeInfo.refresh();
    }

    public static boolean isHighlightable(AccessibilityNodeInfo node) {
        if (!isValidNode(node)) return false;
        if (node.getCollectionInfo() != null) return false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (!node.isImportantForAccessibility()) return false;
        }
        return node.isVisibleToUser() && (node.isFocusable() || node.isClickable());
    }

    public static AccessibilityNodeInfo firstFocusableChild(AccessibilityNodeInfo nodeInfo){
        if(!isValidNode(nodeInfo)) return null;

        int children = nodeInfo.getChildCount();
        for(int i=0; i<children; i++){
            AccessibilityNodeInfo child = nodeInfo.getChild(i);
            if(child != null) Log.d("yolo", "child being explorerd " + child.getClassName());
            AccessibilityNodeInfo firstFocusableNode = firstFocusableNode(child);
            if(isHighlightable(firstFocusableNode)) return firstFocusableNode;
        }
        return null;
    }
    public static AccessibilityNodeInfo siblingPrevious(AccessibilityNodeInfo node) {
        AccessibilityNodeInfo parent = node.getParent();
        if(parent == null) return null;
        int childCount = parent.getChildCount();
        if(childCount == 1) { // no other sibling present
            return siblingPrevious(parent);
        }
        int positionInParent = getIndexInParent(node);
        if(positionInParent == -1) {
            Log.e("yolo", "couldn't find self in parent, bigger problem");
            return parent;
        }
        if(positionInParent == 0) { // last child so no more siblings
            if(isHighlightable(parent)) return parent;
            return siblingPrevious(parent);
        }
        for(int i=positionInParent-1; i>=0; i--) {
            AccessibilityNodeInfo focusableSibling = lastFocusableNode(parent.getChild(i));
            if(focusableSibling == null) continue;
            return focusableSibling;
        }
        return siblingPrevious(parent); // no sibling had focusable nodes
    }

    public static String getText(AccessibilityNodeInfo nodeInfo) {
        if(nodeInfo == null) return "";
        CharSequence text;
        text = nodeInfo.getContentDescription();
        if(!isEmpty(text)) return text.toString();
        text = nodeInfo.getText();
        if(!isEmpty(text)) return text.toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            text = nodeInfo.getTooltipText();
            if(!isEmpty(text)) return text.toString();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            text = nodeInfo.getHintText();
            if(!isEmpty(text)) return text.toString();
        }
        AccessibilityNodeInfo labeledBy = nodeInfo.getLabeledBy();
        if(labeledBy != null) return getText(labeledBy);
        StringBuffer textBuffer = new StringBuffer();
        getAllNonReachableTextUnder(nodeInfo, textBuffer);
        text = textBuffer.toString();
        if(!isEmpty(text)) return text.toString();
        return "Un-labelled " + nodeInfo.getClassName();
    }


    private static void joinTextToBufferWithDot(StringBuffer one, CharSequence two) {
        if(one.length() == 0) one.append(two);
        else one.append(",").append(two);
    }
    public static void getAllNonReachableTextUnder(AccessibilityNodeInfo nodeInfo, StringBuffer text) {
        if(!isValidNode(nodeInfo)) return;
        int childCount = nodeInfo.getChildCount();
        for(int i=0; i<childCount; i++){
            AccessibilityNodeInfo child = nodeInfo.getChild(i);
            if(child == null) continue;
            if(!child.isVisibleToUser()) continue;
            if(isHighlightable(child)) continue;
            if(!isEmpty(child.getContentDescription())) joinTextToBufferWithDot(text, child.getContentDescription());
            else if(!isEmpty(child.getText())) joinTextToBufferWithDot(text, child.getText());
            else getAllNonReachableTextUnder(child, text);
        }
    }
    @Nullable
    public static AccessibilityNodeInfo firstFocusableParent(AccessibilityNodeInfo node) {
        if(!isValidNode(node)) return null;

        AccessibilityNodeInfo parent = node.getParent();
        if(isHighlightable(parent)) return parent;
        return firstFocusableParent(parent);
    }
    @Nullable
    public static AccessibilityNodeInfo lastFocusableNode(AccessibilityNodeInfo node) {
        if(!isValidNode(node)) return null;

        if(isHighlightable(node)) return node;
        return lastFocusableChild(node);
    }


    @Nullable
    public static AccessibilityNodeInfo lastFocusableChild(AccessibilityNodeInfo nodeInfo) {
        if(!isValidNode(nodeInfo)) return null;

        int children = nodeInfo.getChildCount();
        for(int i=children-1; i>=0; i--){
            AccessibilityNodeInfo child = nodeInfo.getChild(i);
            Log.d("yolo", "child being explorerd " + child.getClassName());
            AccessibilityNodeInfo lastFocusableChild = lastFocusableNode(child);
            if(isHighlightable(lastFocusableChild)) return lastFocusableChild;
        }
        return null;
    }

    public static boolean hasText(String text, AccessibilityNodeInfo nodeInfo) {
        return getText(nodeInfo).toLowerCase().contains(text.toLowerCase());
    }

    @Nullable
    public static AccessibilityNodeInfo getSealedInstance(AccessibilityNodeInfo desiredNode, AccessibilityNodeInfo root) {
        if(desiredNode == null) return null;
        if(!isValidNode(root)) return null;
        if(desiredNode.equals(root)) return root;
        int childCount = root.getChildCount();
        if(childCount == 0) return null;
        for(int i=0; i<childCount; i++){
            AccessibilityNodeInfo matchingNode = getSealedInstance(desiredNode, root.getChild(i));
            if(matchingNode != null) return matchingNode;
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void click(float x, float y, Runnable postClick, AccessibilityService accessibilityService) {
        // for a single tap a duration of 1 ms is enough
        final int DURATION = 1;

        Path clickPath = new Path();
        clickPath.moveTo(x, y);
        GestureDescription.StrokeDescription clickStroke =
                new GestureDescription.StrokeDescription(clickPath, 0, DURATION);
        GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
        clickBuilder.addStroke(clickStroke);
        accessibilityService.dispatchGesture(clickBuilder.build(), postGesturePerformCallback(postClick), null);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    private static AccessibilityService.GestureResultCallback postGesturePerformCallback(Runnable postClick) {
        return new AccessibilityService.GestureResultCallback() {
            @Override
            public void onCompleted(GestureDescription gestureDescription) {
                if(postClick !=null) postClick.run();
            }

            @Override
            public void onCancelled(GestureDescription gestureDescription) {
                if(postClick !=null) postClick.run();
            }
        };
    }


    public static List<AccessibilityNodeInfo> getAllNodesThatSatisfy(Predicate<AccessibilityNodeInfo> predicate, AccessibilityNodeInfo root) {
        if(!isValidNode(root)) return Collections.emptyList();
        List<AccessibilityNodeInfo> nodes = new ArrayList<>();
        if(predicate.test(root)) nodes.add(root);
        int childCount = root.getChildCount();
        if(childCount == 0) return nodes;
        for(int i=0; i<childCount; i++){
            nodes.addAll(getAllNodesThatSatisfy(predicate, root.getChild(i)));
        }
        return nodes;
    }

    public static String nodeMetaText(AccessibilityNodeInfo nodeInfo) {
        String text = "";
        if(nodeInfo.getCollectionItemInfo() != null){
            int indexInParent = getIndexInParent(nodeInfo);
            if(indexInParent == -1) return null;
            text = text + " item " + (indexInParent + 1) + " of "+ nodeInfo.getParent().getChildCount();
            if(nodeInfo.getCollectionItemInfo().isSelected()) text = text + " selected";
            if(nodeInfo.getParent().isScrollable()) text = text + " scrollable";
        }
        if(nodeInfo.isEditable()) text = text + " This is editable";
        return text;
    }
    public static Command getInputType(AccessibilityNodeInfo node){
        if(!node.isEditable()) return INPUT_TEXT_TYPE_TEXT;
        int inputType = node.getInputType();
        if(inputType == TYPE_CLASS_PHONE || inputType == TYPE_CLASS_NUMBER) return INPUT_TEXT_TYPE_NUMBER;
        return INPUT_TEXT_TYPE_TEXT;
    }

    public static boolean fillText(String text, AccessibilityNodeInfo node){
        Bundle bundle = new Bundle();
        bundle.putString(ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, text);
        return node.performAction(ACTION_SET_TEXT, bundle);
    }

    @Nullable
    public static AccessibilityNodeInfo getParentOrSelfMatchingCriteria(AccessibilityNodeInfo node, Predicate<AccessibilityNodeInfo> condition){
        if(!isValidNode(node)) return null;
        if(condition.test(node)) return node;
        AccessibilityNodeInfo parent = node.getParent();
        if(parent == null) return null;
        return getParentOrSelfMatchingCriteria(parent, condition);
    }
}
