package com.example.mia.utils;

import android.content.Context;
import android.content.Intent;

import com.example.mia.activities.WorkflowEditActitivty;
import com.example.mia.workflows.Workflow;

public class Domain {

    public static final String WORKFLOW_TITLE_STRING_EXTRA = "WORKFLOW_TITLE";

    public static Intent getIntentToEditWorkflow(Workflow workflow, Context context){
        return new Intent(context, WorkflowEditActitivty.class)
                .putExtra(WORKFLOW_TITLE_STRING_EXTRA, workflow.title);
    }

    public static Intent getIntentToCreateNewWorkflow(Context context){
        return new Intent(context, WorkflowEditActitivty.class);
    }
}
