package com.example.mia.utils;

import static android.os.PowerManager.ACQUIRE_CAUSES_WAKEUP;

import android.content.Context;
import android.os.PowerManager;

public class WakeLockUtils {
    private static PowerManager.WakeLock screenWakeLock;
    private static PowerManager.WakeLock tempScreenWakeLock;
    private static PowerManager.WakeLock cpuWakeLock;
    private static PowerManager.WakeLock dimWakeLock;

    public static void acquireScreenWakeLock(Context context){
        if(isHeld(screenWakeLock)) return;
        screenWakeLock = ((PowerManager) context.getSystemService(Context.POWER_SERVICE))
                .newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | ACQUIRE_CAUSES_WAKEUP, "MakeItAccessible:screen-wake-lock");
        screenWakeLock.acquire();
    }

    public static void acquireTempScreenWakeLock(Context context){
        if(isHeld(tempScreenWakeLock)) return;
        tempScreenWakeLock = ((PowerManager) context.getSystemService(Context.POWER_SERVICE))
                .newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | ACQUIRE_CAUSES_WAKEUP, "MakeItAccessible:screen-temp-wake-lock");
        tempScreenWakeLock.acquire();
    }

    public static void acquireDimScreenWakeLock(Context context) {
        if(isHeld(dimWakeLock)) return;
        dimWakeLock = ((PowerManager) context.getSystemService(Context.POWER_SERVICE))
                .newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | ACQUIRE_CAUSES_WAKEUP, "MakeItAccessible:screen-dim-wake-lock");
        dimWakeLock.acquire();
    }

    private static boolean isHeld(PowerManager.WakeLock wakeLock) {
        return wakeLock != null && wakeLock.isHeld();
    }

    public static void removeTempScreenWakeLock() {
        removeLock(tempScreenWakeLock);
    }

    public static void removeDimScreenWakeLock() {
        removeLock(dimWakeLock);
    }


    public static void removeScreenWakeLock(){
        removeLock(screenWakeLock);
    }

    public static void removeCPUWakeLock() {
        removeLock(cpuWakeLock);
    }

    public static void removeLock(PowerManager.WakeLock wakeLock) {
        if (isHeld(wakeLock)) wakeLock.release();
    }

    public static void acquireCPUWakeLock(Context context){
        if(cpuWakeLock != null && cpuWakeLock.isHeld()) return;
        cpuWakeLock = ((PowerManager) context.getSystemService(Context.POWER_SERVICE))
                .newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MakeItAccessible:cpu-wake-lock");
        cpuWakeLock.acquire();
    }

}
