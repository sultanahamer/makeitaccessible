package com.example.mia.utils;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import android.text.TextUtils;

import java.util.List;

public class Text {
    public static String getCharacters(String text) {
        if(TextUtils.isEmpty(text)) return "";

        String characters = "";
        for(String word : text.split(" ")) {
            characters = characters + word.charAt(0);
        }

        return characters;
    }

    public static boolean isEmpty(CharSequence text) {
        if(text == null) return true;
        if(text.toString().trim().isEmpty()) return true;
        return false;
    }

    public static List<String> words(String text) {
        if(text == null) return emptyList();
        if(text.toString().trim().isEmpty()) return emptyList();
        return asList(text.split(" "));
    }

    public static boolean hasTrailingSpace(String text) {
        return text.charAt(text.length() -1) == ' ';
    }


}
