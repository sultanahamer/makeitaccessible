package com.example.mia.utils;

import static android.speech.tts.TextToSpeech.SUCCESS;
import static android.text.TextUtils.isEmpty;
import static com.example.mia.VoiceInputService.processingWorkflow;
import static com.example.mia.conversations.SleepConversation.isInSleepMode;

import android.speech.tts.TextToSpeech;
import android.util.Log;

public class Tts {
    private static boolean shouldEnableTts(TtsWrapper ttsWrapper){
        if(isInSleepMode()) return ttsWrapper.duringSleep;
        else if(processingWorkflow) return ttsWrapper.duringWorkflow;
        return true;
    }
    public static void cancelAllOnGoingTts(TtsWrapper ttsWrapper) {
        ttsWrapper.tts.stop();
    }

    public static void speakCancellingQueue(CharSequence text, TtsWrapper ttsWrapper) {
        if(!shouldEnableTts(ttsWrapper)) return;
        if(ttsWrapper.tts == null) {
            Log.e("yolo", "tts is null, Couldn't speak text: " + text);
            return;
        }
        if(isEmpty(text)) return;
        Log.d("yolo tts", "speaking " +text);
        int queued = ttsWrapper.tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, "cancelqueue");
        if(queued != SUCCESS) Log.e("yolo tts", "couldn't speak text" +text);
    }

    public static void speakAfterQueue(CharSequence text, TtsWrapper ttsWrapper) {
        speakAfterQueue(text, "queue", ttsWrapper);
    }

    public static void speakAfterQueue(CharSequence text, String utteranceId, TtsWrapper ttsWrapper) {
        if(!shouldEnableTts(ttsWrapper)) return;
        if(ttsWrapper.tts == null) {
            Log.e("yolo", "tts is null, Couldn't speak text: " + text);
            return;
        }
        if(isEmpty(text)) return;
        Log.d("yolo tts", "speaking " +text);
        int queued = ttsWrapper.tts.speak(text, TextToSpeech.QUEUE_ADD, null, utteranceId);
        if(queued != SUCCESS) Log.e("yolo tts", "couldn't speak text" +text);
    }

    public static class TtsWrapper {
        public boolean duringWorkflow = false;
        public boolean duringSleep = false;

        public TextToSpeech tts;
        public TtsWrapper(TextToSpeech tts) {
            this.tts = tts;
        }

        public TextToSpeech getTts(){
            return tts;
        }

    }

    public static TtsWrapper getTtsThatWorksDuringWorkflow(TextToSpeech tts) {
        TtsWrapper ttsWrapper = genericTts(tts);
        ttsWrapper.duringWorkflow = true;
        return ttsWrapper;
    }

    public static TtsWrapper getTtsForSleep(TextToSpeech tts) {
        TtsWrapper ttsWrapper = genericTts(tts);
        ttsWrapper.duringSleep = true;
        return ttsWrapper;
    }

    public static TtsWrapper getTtsForSleep(TtsWrapper ttsWrapper) {
        TtsWrapper ttsWrapperToUse = genericTts(ttsWrapper.tts);
        ttsWrapperToUse.duringSleep = true;
        return ttsWrapperToUse;
    }
    public static TtsWrapper genericTts(TextToSpeech tts) {
        return new TtsWrapper(tts);
    }
}
