package com.example.mia.utils;

import androidx.annotation.Nullable;

import com.github.underscore.Consumer;
import com.github.underscore.Function;

import java.util.List;

public class Java {
    public static int getIndexIgnoringCase(String text, List<String> list){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).equalsIgnoreCase(text)) return i;
        }
        return -1;
    }
    @Nullable
    public static String getMatchingIgnoringCase(String text, List<String> list){
        for(String currentItem : list){
            if(currentItem.equalsIgnoreCase(text)) return currentItem;
        }
        return null;
    }

    public static void safeCall(FunctionThatThrows functionThatThrows, Consumer<Exception> onFailure) {
        try{
            functionThatThrows.run();
        }
        catch (Exception e){
            e.printStackTrace();
            onFailure.accept(e);
        }
    }

    public static <I, O> O safeFunction(Function<I, O> func, I arg, Function<Exception, O> onFailure) {
        try{
            return func.apply(arg);
        }
        catch (Exception e){
            e.printStackTrace();
            return onFailure.apply(e);
        }
    }

    public interface FunctionThatThrows {
        void run() throws Exception;
    }
}
