package com.example.mia.utils;

import static java.util.Arrays.asList;

import androidx.annotation.Nullable;

import com.github.underscore.U;

import java.util.List;

public class Digits {
    public static final List<String> digits = asList("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten");
    private static String numberRegex = "^\\d+$" ;
    @Nullable
    public static String recognizeDigit(String text){
        String digit = U.last(text.split(" "));
        int index = digits.indexOf(digit);
        if(index == -1) {
            return null;
        }
        return digits.get(index);
    }
    public static Integer textToDigit(String digitAsText) {
        Integer digit = digits.indexOf(digitAsText);
        return digit == -1 ? null : digit;
    }

    @Nullable
    public static Long getNumber(String text) {
        if (text.matches(numberRegex)) {
            try {
                return Long.parseLong(text);
            } catch (Exception e) { }
        }
        String digitText = recognizeDigit(text);
        if(digitText == null) return null;
        Integer digit = textToDigit(digitText);
        if(digit == null) return null;
        return digit.longValue();
    }

    public static Integer getLastDigit(String text) {
        if (text.matches(numberRegex)) {
            try {
                return Integer.parseInt(text) % 10;
            } catch (Exception e) { }
        }
        String digitText = recognizeDigit(text);
        if(digitText == null) return null;
        return textToDigit(digitText);

    }
}
