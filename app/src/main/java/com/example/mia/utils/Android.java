package com.example.mia.utils;

import static com.example.mia.utils.Async.runOnMain;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;

public class Android {
    public static boolean launchApp(String packageName, Context context) {
        try {
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(packageName);
            if (launchIntentForPackage == null) return false;
            context.startActivity(launchIntentForPackage);
        } catch (Exception e) {
            Log.e("yolo app launch", "failed launching app " + e.getMessage());
            return false;
        }
        return true;
    }
    public static void setBackButtonInToolBar(Toolbar toolBar, final AppCompatActivity appCompatActivity) {
        toolBar.setNavigationOnClickListener(v -> appCompatActivity.onBackPressed());
//        Drawable navigationIcon = toolBar.getNavigationIcon();
//        if (navigationIcon == null)
//            return;
//        setColorFilterUsingColor(navigationIcon, getPrimaryColor(appCompatActivity));
    }

    public static String getText(TextInputEditText node){
        Editable editable = node.getText();
        if (editable == null) return "";
        return editable.toString();
    }

    public static String getApplicationName(String packageName, Context context) {
        try {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo applicationInfo = pm.getApplicationInfo(packageName, 0);
            return pm.getApplicationLabel(applicationInfo).toString();
        } catch (Exception e) {
            System.out.println("This application has no name hence using its package name" + packageName);
            return packageName;
        }
    }

    public static void showToastError(String message, Context context){
        runOnMain(() -> Toast.makeText(context, message, Toast.LENGTH_LONG).show());
    }

    public static void announceError(String message, TextToSpeech tts) {
        if(tts == null) {
            Log.e("error handling",  "tts is null while announcing " + message);
            return;
        }
        tts.speak(message, TextToSpeech.QUEUE_ADD, null, "error");
    }
}
