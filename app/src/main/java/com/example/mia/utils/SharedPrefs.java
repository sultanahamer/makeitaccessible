package com.example.mia.utils;

import static android.content.Context.MODE_PRIVATE;

import static java.util.Collections.emptySet;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.mia.appactions.AppAction;
import com.github.underscore.U;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SharedPrefs {
    private static final String COMMON_SHARED_PREFS_FILE_NAME = "common";
    private static final String WORKFLOWS_SHARED_PREFS_FILE_NAME = "workflows";
    private static final String WORKFLOWS_SHARED_PREFS_KEY = "workflows_list";
    private static final String APP_ACTIONS_SHARED_PREFS_FILE_NAME = "app_actions";
    private static final String APP_ACTIONS_PREFIX = "app_actions_";
    public static final String APP_ACTIONS_APPS_LIST = "APP_ACTIONS_APPS_LIST";

    public static List<String> getAllStringifiedWorkflows(Context context) {
        SharedPreferences preferences = getSharedPreferences(WORKFLOWS_SHARED_PREFS_FILE_NAME, context);
        return new ArrayList<>(preferences.getStringSet("workflows", new HashSet<>()));
    }

    public static void saveAllStringifiedWorkflows(List<String> strigifiedWorkflows, Context context) {
        SharedPreferences preferences = getSharedPreferences(WORKFLOWS_SHARED_PREFS_FILE_NAME, context);
        preferences.edit().putStringSet("workflows", new HashSet<>(strigifiedWorkflows)).apply();
    }

    private static SharedPreferences getSharedPreferences(String fileName, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(fileName, MODE_PRIVATE);
        return preferences;
    }

    public static void saveActions(List<AppAction> actions, String packageName, Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(APP_ACTIONS_SHARED_PREFS_FILE_NAME, context);
        Set<String> appsList = new HashSet<>(sharedPreferences.getStringSet(APP_ACTIONS_APPS_LIST, emptySet()));
        appsList.add(packageName);
        sharedPreferences.edit().putStringSet(APP_ACTIONS_APPS_LIST, appsList).apply();
        Gson gson = new Gson();
        String json = gson.toJson(actions);
        sharedPreferences.edit().putString(APP_ACTIONS_PREFIX + packageName, json).apply();
    }

    private static List<AppAction> getAppActionsForApp(String appName, SharedPreferences sharedPrefs) {
        String stringifiedAppActionsList = sharedPrefs.getString(APP_ACTIONS_PREFIX + appName, "[]");
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<AppAction>>(){}.getType();
        List<AppAction> appActions = gson.fromJson(stringifiedAppActionsList, listType);
        return appActions;
    }
    public static List<AppAction> getAllAppActions(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(APP_ACTIONS_SHARED_PREFS_FILE_NAME, context);
        Set<String> appsList = sharedPreferences.getStringSet(APP_ACTIONS_APPS_LIST, emptySet());
        return U.flatten(
                U.map(new ArrayList<>(appsList), appName -> getAppActionsForApp(appName, sharedPreferences))
        );
    }

//    public static String getAppIdentifier(String packageName, Context context) {
//        return getSharedPreferences(context).getString("APP_IDENTIFIER_" + packageName, "");
//    }
}
