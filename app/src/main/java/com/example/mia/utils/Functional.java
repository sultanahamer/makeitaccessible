package com.example.mia.utils;

import com.github.underscore.Function;

import java.util.ArrayList;
import java.util.List;

public class Functional {
    public static <T> List<T> mapIndexes(int count, Function<Integer, T> func) {
        ArrayList<T> results = new ArrayList<>(count);
        for(int i=0; i<count; i++) {
            results.add(func.apply(i));
        }
        return results;
    }
}
