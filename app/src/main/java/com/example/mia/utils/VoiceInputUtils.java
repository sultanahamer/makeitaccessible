package com.example.mia.utils;

import static com.example.mia.Command.INPUT_TEXT_TYPE_CHARACTER;
import static com.example.mia.Command.INPUT_TEXT_TYPE_NUMBER;
import static com.example.mia.utils.Digits.getNumber;
import static com.example.mia.utils.Text.getCharacters;
import static com.example.mia.utils.Text.words;

import android.content.Context;
import android.text.TextUtils;

import com.example.mia.Command;
import com.github.underscore.U;

import java.util.List;

public class VoiceInputUtils {
    public static String processSpecialInput(String input, Context context) {
        if(TextUtils.isEmpty(input)) return "";
        List<String> words = words(input);
        if(words.size() == 1) return input;
        String codeWord = words.get(0);
        String inputExcludingInputType = U.join(U.rest(words), " "); // excluding alpha/number from input
        if(Command.getTextFor(INPUT_TEXT_TYPE_CHARACTER, context).equals(codeWord)) return getCharacters(inputExcludingInputType);
        else if (Command.getTextFor(INPUT_TEXT_TYPE_NUMBER, context).equals(codeWord)) return getNumber(inputExcludingInputType) + "";
        return input;
    }
}
