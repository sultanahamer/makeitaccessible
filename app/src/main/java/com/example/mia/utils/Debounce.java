package com.example.mia.utils;

import static com.example.mia.utils.Async.runOnMainDelayed;

import android.util.Log;

public class Debounce {
    private final Runnable process;
    private final int debounceIntervalInMillis;
    private long runAfter = 0;
    private long lastRan = 1;

    public Debounce(Runnable process, int debounceInterval) {
        this.debounceIntervalInMillis = debounceInterval;
        this.process = process;
    }

    private void probablyRun(){ // don't call this directly
        Log.d("debounce", "pending time is " + (runAfter - System.currentTimeMillis()) + " millis");
        if(System.currentTimeMillis() < runAfter) {
            runOnMainDelayed(this::probablyRun, debounceIntervalInMillis);
            return;
        };
        try{
            process.run();
        }
        catch (Exception e) {
            Log.e("debounced", "error while running debounced func" + e.getMessage());
            e.printStackTrace();
        }
        lastRan = System.currentTimeMillis();
        if(runAfter >= lastRan) runOnMainDelayed(this::probablyRun, debounceIntervalInMillis);
    }

    public void debouncedRun(){
        if(lastRan >= runAfter) runOnMainDelayed(this::probablyRun, debounceIntervalInMillis);
        runAfter = System.currentTimeMillis() + debounceIntervalInMillis;
        Log.d("debounce", "scheduling debounce at" + runAfter + " millis");
    }

    public void cancelRun(){
        runAfter = Long.MAX_VALUE;
    }

}
