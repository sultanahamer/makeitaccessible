package com.example.mia.utils;

import androidx.annotation.Nullable;
import androidx.core.util.Predicate;

import java.util.List;

public class ListUtils {
    @Nullable
    public static <T> T find(List<T> things, Predicate<T> predicate) {
        for(T thing: things){
            if(predicate.test(thing)) return thing;
        }
        return null;
    }
}
