package com.example.mia.utils;

import static com.example.mia.activities.AddAppActionsActivity.ACTION_IDENTIFIER;
import static com.example.mia.activities.AddAppActionsActivity.ACTION_TRIGGER_INPUT;
import static com.example.mia.activities.AddAppActionsActivity.CHOICE_INDEX_EXTRA;
import static com.example.mia.activities.AddAppActionsActivity.CHOOSE_ACTION;
import static com.example.mia.command.CommandUtils.getVariables;
import static com.example.mia.command.CommandUtils.hasVariables;
import static java.util.Arrays.asList;

import android.content.Context;
import android.content.Intent;


import com.example.mia.appactions.AppAction;

import java.util.ArrayList;
import java.util.List;

public class AppActionUtils {

    public static void trigger(String input, AppAction action, Context context){
        List<String> variables = getVariables(action.defaultVoiceInput, input, context);
        context.sendBroadcast(new Intent("MIA_ACTION")
                .setPackage(action.appPackageName)
                .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                .putExtra(ACTION_IDENTIFIER, action.identifier)
                .putExtra(ACTION_TRIGGER_INPUT, input)
                .putStringArrayListExtra("ACTION_TRIGGER_VARIABLES", new ArrayList<>(variables))
        );
    }

    public static void triggerNaansAction(AppAction action, Context context){
        context.sendBroadcast(new Intent("MIA_ACTION")
                .setPackage(action.appPackageName)
                .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                .putExtra(ACTION_IDENTIFIER, action.identifier)
        );
    }

    public static void conveyChoice(int index, AppAction action, Context context){
        context.sendBroadcast(new Intent(CHOOSE_ACTION)
                .setPackage(action.appPackageName)
                .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                .putExtra(ACTION_IDENTIFIER, action.identifier)
                .putExtra(CHOICE_INDEX_EXTRA, index)
        );
    }

}
