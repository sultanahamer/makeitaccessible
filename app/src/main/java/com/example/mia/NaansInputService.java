package com.example.mia;

import static com.example.mia.conversations.SleepConversation.*;
import static com.example.mia.conversations.naans.NaansChoice.uponChoosing;
import static com.example.mia.utils.AppActionUtils.conveyChoice;
import static com.example.mia.utils.AppActionUtils.triggerNaansAction;
import static com.example.mia.utils.Async.runAsync;
import static com.example.mia.utils.MessageBus.MessageType.APP_ACTION_CHOOSE_REQUEST;
import static com.example.mia.workflows.AppActionsStore.getNaansActions;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;

import com.example.mia.appactions.AppAction;
import com.example.mia.conversations.ConversationProcessor;
import com.example.mia.utils.MessageBus;
import com.example.mia.utils.Tts;
import com.example.mia.utils.WakeLockUtils;
import com.github.underscore.Consumer;

import java.lang.ref.WeakReference;
import java.util.List;

public class NaansInputService {
    private TextToSpeech tts;
    private WeakReference<NaansAccessibilityService> accessibilityServiceRef;
    private static AppAction currentAction;
    private static Handler inputHandlingThread;

    private final Consumer<Object> onChooseRequest = o -> {
        List<String> choices = (List<String>) o;
        uponChoosing(choices, choice -> conveyChoice(choices.indexOf(choice), currentAction, accessibilityServiceRef.get()), () ->{
            conveyChoice(-1, currentAction, accessibilityServiceRef.get());
        }, Tts.genericTts(tts), accessibilityServiceRef.get());

    };
    private BroadcastReceiver bluetoothReceiver;

    NaansInputService(NaansAccessibilityService service, Runnable onReady, TextToSpeech tts) {
        this.tts = tts;
        accessibilityServiceRef = new WeakReference<>(service);
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                inputHandlingThread = new Handler(Looper.myLooper());
                runAsync(onReady::run);
                Looper.loop();
            }
        }.start();
        MessageBus.subscribe(APP_ACTION_CHOOSE_REQUEST, onChooseRequest);
        start(service, tts);
        listenForBluetoothConnections(service);
    }

    private void start(NaansAccessibilityService service, TextToSpeech tts) {
        uponChoosing(getNaansActions(accessibilityServiceRef.get()), action -> action.announceText, action -> {
            Log.d("naans input",  "chose " + action.identifier);
            currentAction = action;
            triggerNaansAction(action, accessibilityServiceRef.get());
        }, () -> {}, Tts.genericTts(tts), service);
    }

    public boolean onPressed(KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN) return true;
        inputHandlingThread.post(() -> handleInput(event));
        return true;
    }

    private void handleInput(KeyEvent event) {
        if(!ConversationProcessor.conversationsGoingOn()) { // TODO: what about waiting for trigger execution
            start(accessibilityServiceRef.get(), tts);
            return;
        }

        ConversationProcessor.processNextInput("");
    }

    public void stop() {
        MessageBus.unsubscribe(APP_ACTION_CHOOSE_REQUEST, onChooseRequest);
        if(inputHandlingThread != null) {
            inputHandlingThread.getLooper().quit();
        }
        if(bluetoothReceiver != null) accessibilityServiceRef.get().unregisterReceiver(bluetoothReceiver);
    }

    public void listenForBluetoothConnections(Context context){

        bluetoothReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                    Log.d("bluetooth", "connected");
                    if(!isOurInputDevice(device)) return;
                    if(ConversationProcessor.conversationsGoingOn()) return;
                    acquireScreenLockTemporarilyAndUnlock(accessibilityServiceRef.get());
                    start(accessibilityServiceRef.get(), tts);
                    WakeLockUtils.acquireDimScreenWakeLock(context);
                }
                else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                }
                else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
                }
                else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                    if(!isOurInputDevice(device)) return;
                    WakeLockUtils.removeDimScreenWakeLock();
                }
            }

        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        context.registerReceiver(bluetoothReceiver, filter);
    }

    private boolean isOurInputDevice(BluetoothDevice device) {
        return true;
    }
}
