package com.example.mia;

import static com.example.mia.Command.DO_NOTHING;
import static com.example.mia.utils.Async.runAsync;

import com.example.mia.workflows.Action;
import com.example.mia.workflows.Workflow;
import com.github.underscore.lodash.U;

import java.util.ArrayList;
import java.util.List;

public class VoiceAutomationInputService {
    private static List<Action> actions = new ArrayList<>();
    private static Runnable onVoiceCommandsAvailable;

    public static Action getNextAction(){
        if(actions.isEmpty()) return new Action(DO_NOTHING, "");

        return actions.remove(0);
    }
    public static boolean hasAutoCommands(){
        return !actions.isEmpty();
    }

    public static void run(Workflow workflow){
        if(workflow.actions.isEmpty() || onVoiceCommandsAvailable == null){
            return;
        }
        if (hasAutoCommands()) {
            actions.addAll(0, workflow.actions);
            return;
        }
        actions.addAll(workflow.actions);
        runAsync(onVoiceCommandsAvailable);
    }

    public static void cleanUp(){
        actions.clear();
        onVoiceCommandsAvailable = null;
    }

    public static void setOnVoiceCommandsAvailable(Runnable runnable){
        onVoiceCommandsAvailable = runnable;
    }

    public static void repeat(String commandToRepeat, int times) {
        List<Action> actions = U.map(U.range(times), i -> new Action(commandToRepeat));
        run(new Workflow("ignore", actions));
    }

    public static void cancel() {
        actions.clear();
    }
}
