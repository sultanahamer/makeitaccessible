package com.example.mia.appactions;

import static com.example.mia.activities.AddAppActionsActivity.ACTION_IDENTIFIER;
import static com.example.mia.activities.AddAppActionsActivity.ANNOUNCE_ERROR_ACTION;
import static com.example.mia.activities.AddAppActionsActivity.APP_IDENTIFIER;
import static com.example.mia.activities.AddAppActionsActivity.CHOICES_EXTRA;
import static com.example.mia.activities.AddAppActionsActivity.CHOOSE_ACTION;
import static com.example.mia.activities.AddAppActionsActivity.ERROR_MESSAGE;
import static com.example.mia.utils.Java.safeCall;
import static com.example.mia.utils.MessageBus.MessageType.APP_ACTION_CHOOSE_REQUEST;
import static com.example.mia.utils.MessageBus.MessageType.APP_ACTION_ERROR;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.mia.VoiceInputService;
import com.example.mia.utils.MessageBus;

import java.util.ArrayList;

public class AppActionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        safeCall(() -> {
//            if(!isValidActionToHandle(intent)) return; // TODO: enable this
            handle(intent);
        }, e -> {
            e.printStackTrace();
            MessageBus.broadcast(APP_ACTION_ERROR, "Error while handling app action");
        });
    }

    private static void handle(Intent intent) {
        if(CHOOSE_ACTION.equals(intent.getAction())) {
            ArrayList<String> choices = intent.getStringArrayListExtra(CHOICES_EXTRA);
            if(choices == null || choices.isEmpty()) return;
            MessageBus.broadcast(APP_ACTION_CHOOSE_REQUEST, choices);
            return;
        }

        if(ANNOUNCE_ERROR_ACTION.equals(intent.getAction())) {
            String errorMessage = intent.getStringExtra(ERROR_MESSAGE);
            MessageBus.broadcast(APP_ACTION_ERROR, errorMessage);
            return;
        }
    }

    private static boolean isValidActionToHandle(Intent intent) {
        AppAction onGoingAppAction = VoiceInputService.getOnGoingAppAction();
        if(onGoingAppAction == null) return false;
        if(!onGoingAppAction.appIdentifier.equals(intent.getStringExtra(APP_IDENTIFIER))) return false;
        if(!onGoingAppAction.identifier.equals(intent.getStringExtra(ACTION_IDENTIFIER))) return false;
        return true;
    }
}
