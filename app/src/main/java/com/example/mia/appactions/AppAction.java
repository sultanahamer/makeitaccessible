package com.example.mia.appactions;

public class AppAction {
    public String appName;
    public String appPackageName;
    public String appIdentifier;

    public String displayName;
    public String description;
    public String defaultVoiceInput;
    public String identifier;
    public String announceText = "default announce text";
    public ActionType actionType = ActionType.VOICE;
    public enum ActionType {
        VOICE, NAANS
    }

    public AppAction(String appName, String appPackageName, String appIdentifier, String displayName, String description, String defaultVoiceInput, String identifier, ActionType type, String announceText) {
        this.appName = appName;
        this.appPackageName = appPackageName;
        this.appIdentifier = appIdentifier;
        this.displayName = displayName;
        this.description = description;
        this.defaultVoiceInput = defaultVoiceInput;
        this.identifier = identifier;
        this.actionType = type;
        this.announceText = announceText;
    }
}
