package com.example.mia;

import java.util.List;

public interface StoreChangeListener<T> {
    default void onAdd(T item) {

    }
    default void delete(T item) {

    }
    default void refresh(List<T> items) {

    }
}
