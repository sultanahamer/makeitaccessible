package com.example.mia;

import com.example.mia.utils.Tts;

public class CommandAndArguments {
    public Command command;
    public String param;
    public String triggerVoiceInput;
    public Tts.TtsWrapper ttsWrapper;

    public CommandAndArguments(Command command, String param, Tts.TtsWrapper ttsWrapper) {
        this.command = command;
        this.param = param;
        this.ttsWrapper = ttsWrapper;
    }

    public CommandAndArguments(Command command, String param, String triggerVoiceInput, Tts.TtsWrapper ttsWrapper) {
        this.command = command;
        this.param = param;
        this.ttsWrapper = ttsWrapper;
        this.triggerVoiceInput = triggerVoiceInput;
    }

}
