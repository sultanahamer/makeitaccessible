package com.example.mia;

import org.junit.Test;

import static org.junit.Assert.*;

import android.text.TextUtils;

import java.util.Arrays;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(true, "hello :1".matches(".*:\\d.*"));
    }
}